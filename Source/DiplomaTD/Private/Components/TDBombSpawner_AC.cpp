// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/TDBombSpawner_AC.h"
#include "Weapon/TDWeapon.h"
#include "Kismet/GameplayStatics.h"
#include "Projectile/TDBombProjectile.h"

// Sets default values for this component's properties
UTDBombSpawner_AC::UTDBombSpawner_AC()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
	bWantsInitializeComponent = true;
	bAutoActivate = true;
	// ...
}


void UTDBombSpawner_AC::SpawnBomb(FVector Location, FVector Direction)
{
	if(CurrentBombCount > 0)
	{
		FTransform SpawnTM(Direction.ToOrientationRotator(), Location);
		ATDBombProjectile* Projectile = Cast<ATDBombProjectile>(UGameplayStatics::BeginDeferredActorSpawnFromClass(this, BombClass, SpawnTM));
		if (Projectile)
		{
			Projectile->SetInstigator(GetOwner()->GetInstigator());
			Projectile->SetOwner(GetOwner());
			Projectile->InitVelocity(Direction);

			UGameplayStatics::FinishSpawningActor(Projectile, SpawnTM);
		}
		CurrentBombCount--;
	}
	
}

int32 UTDBombSpawner_AC::GetCurrentBombCount() const
{
	return CurrentBombCount;
}

int32 UTDBombSpawner_AC::GetMaxBombCount() const
{
	return MaxBombCount;
}

void UTDBombSpawner_AC::InitializeComponent()
{
	CurrentBombCount = MaxBombCount;
	Super::InitializeComponent();
}






