// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/NPC/NPC.h"
#include "GameFramework/Controller.h"
#include "TDShooter.h"

// Sets default values
ANPC::ANPC()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

FTDAttribute ANPC::GetHealthAttribute_Implementation() const
{
	return Health;
}

bool ANPC::IsAlive_Implementation() const
{
	return Health.CurrentValue > 0;
}

int32 ANPC::GetPointsForMyHead_Implementation() const
{
	return ScoreForMyHead;
}

void ANPC::MeleeAttack_Implementation()
{
	UAnimMontage* CurMontage = Execute_GetCurrentMeleeAnimMontage(this);
	if (ensure(CurMontage))
	{
		PlayAnimMontage(CurMontage);
	}
}

UAnimMontage* ANPC::GetCurrentMeleeAnimMontage_Implementation() const
{
	return DefaultMeleeMontage;
}

float ANPC::GetCurrentMeleeAnimMontageLength_Implementation() const
{
	return ITDCombatInterface::GetCurrentMeleeAnimMontageLength_DEFAULT(this);
}

float ANPC::GetMeleeDamage_Implementation() const
{
	return MeleeDamage;
}

void ANPC::ModifyHealth_Implementation(float Coefficient)
{
	Health.BaseValue *= Coefficient;
	Health.CurrentValue *= Coefficient;
}


void ANPC::ModifyDamage_Implementation(float Coefficient)
{
	MeleeDamage *= Coefficient;
}

void ANPC::BeginPlay()
{
	Super::BeginPlay();
	Health.CurrentValue = Health.BaseValue;
}

float ANPC::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
                       AActor* DamageCauser)
{
	float Dmg = 0.f;
	if (Health.CurrentValue > 0.f)
	{
		Dmg = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
		if (Dmg > 0.f)
		{
			Health.CurrentValue -= Dmg;
			if (Health.CurrentValue <= 0.f)
			{
				Die(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
				DetachFromControllerPendingDestroy();
				SetLifeSpan(10.f);
			}
		}
	}
	
	return Dmg;
}

void ANPC::Die(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	Health.CurrentValue = 0.f;
	const APawn* InstigatorPawn = EventInstigator ? EventInstigator->GetPawn() : nullptr;
	if (InstigatorPawn && (InstigatorPawn->Implements<UTDShooter>() || Cast<ITDShooter>(InstigatorPawn)))
	{
		ITDShooter::Execute_ScoreKill(EventInstigator->GetPawn(), this, ITDAbleToDie::Execute_GetPointsForMyHead(this));
	}

	//@todo dlavoryk: play Death Animation
}

void ANPC::Attack_Implementation()
{
	check(false);
}
