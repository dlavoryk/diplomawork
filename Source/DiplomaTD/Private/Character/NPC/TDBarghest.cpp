// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/NPC/TDBarghest.h"

FString ExpandEnumString(const FString& name, const FString& enumName);

template<typename T>
FString EnumToString(const FString& enumName, const T value, const FString& defaultValue)
{
	UEnum* pEnum = FindObject<UEnum>(ANY_PACKAGE, *enumName, true);
	return pEnum
		? ExpandEnumString(pEnum->GetNameByIndex(static_cast<uint8>(value)).ToString(), enumName)
		: defaultValue;
}

FString ExpandEnumString(const FString& name, const FString& enumName)
{
	FString expanded(name);
	FString spaceLetter("");
	FString spaceNumber("");
	FString search("");
	expanded.ReplaceInline(*enumName, TEXT(""), ESearchCase::CaseSensitive);
	expanded.ReplaceInline(TEXT("::"), TEXT(""), ESearchCase::CaseSensitive);
	for (TCHAR letter = 'A'; letter <= 'Z'; ++letter)
	{
		search = FString::Printf(TEXT("%c"), letter);
		spaceLetter = FString::Printf(TEXT(" %c"), letter);
		expanded.ReplaceInline(*search, *spaceLetter, ESearchCase::CaseSensitive);
	}
	for (TCHAR number = '0'; number <= '9'; ++number)
	{
		search = FString::Printf(TEXT("%c"), number);
		spaceNumber = FString::Printf(TEXT(" %c"), number);
		expanded.ReplaceInline(*search, *spaceNumber, ESearchCase::CaseSensitive);
	}
	expanded.ReplaceInline(TEXT("_"), TEXT(" -"), ESearchCase::CaseSensitive);
	expanded = expanded.RightChop(1).Trim().TrimTrailing();
	return expanded;
}


void ATDBarghest::BeginPlay()
{
	Super::BeginPlay();
	check(MeleeAttackMontage.Num() == static_cast<int32>(ETDBarghestBiteMontage::bbmMAX));
	
	OnTakeAnyDamage.AddUniqueDynamic(this, &ATDBarghest::OnTakeAnyDamage_Impl);
}

bool ATDBarghest::CanBite() const
{
	return true;
}

bool ATDBarghest::IsAggressive() const
{
	return bIsAggressive;
}

void ATDBarghest::SetIsAggressive(bool IN_bIsAggressive)
{
	bIsAggressive = IN_bIsAggressive;
}

ETDBarghestIdleState ATDBarghest::GetLastIdleState() const
{
	return ETDBarghestIdleState::bsIdleBreath;
}

bool ATDBarghest::HasCriticalHP() const
{
	return (Health.BaseValue - 2 * Health.CurrentValue) > 0;
}

UAnimMontage* ATDBarghest::GetCurrentMeleeAnimMontage_Implementation() const
{
	const ETDBarghestBiteMontage CurMontage = GetDesiredMontage();
	UE_LOG(LogTemp, Warning, TEXT("%s"), *EnumToString<ETDBarghestBiteMontage>(TEXT("ETDBarghestBiteMontage"), CurMontage, TEXT("DEFAULT_VALUE")));
	return MeleeAttackMontage[CurMontage];
}

ETDBarghestBiteMontage ATDBarghest::GetDesiredMontage() const
{
	const bool bIsMoving = GetVelocity().SizeSquared() > 1.f;
	if (IsAggressive())
	{
		if (bIsMoving)
		{
			return ETDBarghestBiteMontage::bbmIdleBiteAggressive;
		}
		else
		{
			return ETDBarghestBiteMontage::bbmIdleBiteAggressive;
		}
	}
	else
	{
		if (bIsMoving)
		{
			return ETDBarghestBiteMontage::bbmRunBite;
		}
		else
		{
			return ETDBarghestBiteMontage::bbmIdleBite;
		}
	}
	

//	return IsAggressive() ? 
	return ETDBarghestBiteMontage::bbmJumpBite;
}

void ATDBarghest::OnTakeAnyDamage_Impl(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
	AController* InstigatedBy, AActor* DamageCauser)
{
	if (IsAlive_Implementation() && HasCriticalHP())
	{
		SetIsAggressive(true);
	}
}
