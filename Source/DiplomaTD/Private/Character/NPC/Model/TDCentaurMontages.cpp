// Fill out your copyright notice in the Description page of Project Settings.
#include "Character/NPC/Model/TDCentaurMontages.h"

const FTDWeaponTransition* UTDCentaurMontages::FindTransition(ETDCentaurWeaponState From,
                                                              ETDCentaurWeaponState To) const
{
	return WeaponStateToMontage.FindByPredicate([From, To](const FTDWeaponTransition& Transition)
		{
			return Transition.From == From && To == Transition.To;
		});
}
