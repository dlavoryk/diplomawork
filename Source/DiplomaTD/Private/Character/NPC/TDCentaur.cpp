// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/NPC/TDCentaur.h"
#include "Components/SkeletalMeshComponent.h"
#include "Character/NPC/Model/TDCentaurMontages.h"
#include "Kismet/GameplayStatics.h"
#include "Projectile/TDProjectile.h"

ATDCentaur::ATDCentaur()
{
	BowAction = CreateDefaultSubobject<USkeletalMeshComponent>("BOW_ACTION");
	Arrow     = CreateDefaultSubobject<USkeletalMeshComponent>("ARROW_ACTION");
	BodyArmor = CreateDefaultSubobject<USkeletalMeshComponent>("BODY_ARMOR");
	BowIdle   = CreateDefaultSubobject<USkeletalMeshComponent>("BOW_IDLE");

	BowAction->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform);
	Arrow    ->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform);
	BodyArmor->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform);
	BowIdle  ->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform);
}

void ATDCentaur::ToggleAim(bool IsAiming)
{
	SetWeaponState(ETDCentaurWeaponState::cwsBowAiming);
}

void ATDCentaur::UseBow()
{
	SetWeaponState(ETDCentaurWeaponState::cwsBowAiming);
}

void ATDCentaur::SetIsBowActive(bool bIsActive)
{
	if (bIsActive != IsBowActive())
	{
		if (bIsActive)
		{
			UseBow();
		}
		else
		{
			SetWeaponState(ETDCentaurWeaponState::cwsNormal);
		}
	}
}

void ATDCentaur::Shoot()
{
	if (IsStateGoodForShooting())
	{
		Shoot_Internal();
	}
	else
	{
		UseBow();
		ToggleAim(true);
		SetWeaponState(ETDCentaurWeaponState::cwsBowAiming);
	}
}

ETDCentaurWeaponState ATDCentaur::GetCentaurWeaponState() const
{
	return mCentaurWeaponState;
}

bool ATDCentaur::IsStateGoodForShooting() const
{
	return mCentaurWeaponState == ETDCentaurWeaponState::cwsBowAiming && const_cast<ATDCentaur*>(this)->GetCurrentMontage() == nullptr;
}

void ATDCentaur::UpdateComponentsVisibility()
{
	const bool bIsBowActive = IsBowActive();
	
	BowAction->SetVisibility(bIsBowActive);
	Arrow->SetVisibility(bIsBowActive);

	BowIdle->SetVisibility(!bIsBowActive);
	
}

void ATDCentaur::BeginPlay()
{
	Super::BeginPlay();
	check(MyAnimMontages && MyAnimMontages);
	
	BowAction->SetMasterPoseComponent(GetMesh());
	Arrow    ->SetMasterPoseComponent(GetMesh());
	BodyArmor->SetMasterPoseComponent(GetMesh());
	BowIdle  ->SetMasterPoseComponent(GetMesh());

	UpdateComponentsVisibility();
}

void ATDCentaur::MeleeAttack_Implementation()
{
	Shoot();
}

void ATDCentaur::Shoot_Internal()
{
	if (ensure(MyAnimMontages->ShootArrowMontage))
	{
		PlayAnimMontage(MyAnimMontages->ShootArrowMontage);
	}

	// Get Arrow socket to specify precise location and direction of the arrow projectile
	const FTransform SpawnTM = GetMesh()->GetSocketTransform(ArrowActionSocket_Name);

	ATDProjectile* Projectile = Cast<ATDProjectile>(UGameplayStatics::BeginDeferredActorSpawnFromClass(this, ArrowClass, SpawnTM));
	if (Projectile)
	{
		Projectile->SetInstigator(GetInstigator());
		Projectile->SetOwner(this);
		Projectile->InitVelocity(SpawnTM.GetRotation().Vector());

		UGameplayStatics::FinishSpawningActor(Projectile, SpawnTM);
	}
}

void ATDCentaur::SetWeaponState(ETDCentaurWeaponState NewState)
{
	if (mCentaurWeaponState != NewState)
	{
		const FTDWeaponTransition* TransitionPtr = MyAnimMontages->FindTransition(mCentaurWeaponState, NewState);
		
		if ((TransitionPtr) && (TransitionPtr->Montage))
		{
			PlayAnimMontage(TransitionPtr->Montage);
		}
		mCentaurWeaponState = NewState;
		UpdateComponentsVisibility();
	}
}
