// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/TDCharacter.h"
#include "Components/InputComponent.h"
#include "Components/CapsuleComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/TDBombSpawner_AC.h"
#include "Weapon/TDWeapon.h"

#include "Engine/StaticMeshActor.h"
#include "Kismet/GameplayStatics.h"
#include "TimerManager.h"

#include "GameFramework/PlayerState.h"
#include "DiplomaTD/Core/DiplomaTDGameMode.h"
#include "GameFramework/PawnMovementComponent.h"

// Sets default values
ATDCharacter::ATDCharacter()
{
	// Set size  for Collision Capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.f);

	//Set out turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	//Create a camera component
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonComponent"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); //Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->SetRelativeRotation(FRotator(1.9f, -19.19f, 5.2f));
	Mesh1P->SetRelativeLocation(FVector(-0.5f, -4.4f, -155.7f));

	//Create BombSpawner component
	BombSpawnerComponent = CreateDefaultSubobject<UTDBombSpawner_AC>(TEXT("BombSpawnerComponent"));


	bIsFiring = false;
	
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;// true;

}

bool ATDCharacter::IsFiring() const
{
	return bIsFiring;
}

bool ATDCharacter::CanDie(float KillingDamage, FDamageEvent const& DamageEvent, AController* Killer,
	AActor* DamageCauser) const
{
	if (
		bIsDying										// already dying
		|| IsPendingKill()								// already destroyed
		)	
	{
		return false;
	}
	return true;
}


bool ATDCharacter::Die(float KillingDamage, FDamageEvent const& DamageEvent, AController* Killer, AActor* DamageCauser)
{
	bool Died = false;
	if (CanDie(KillingDamage, DamageEvent, Killer, DamageCauser))
	{
		Died = true;
		Health.CurrentValue = 0.f;

		TearOff();

		
		GetMovementComponent()->StopMovementImmediately();
		DetachFromControllerPendingDestroy();
		//StopAllAnimMontages();
		if (GetMesh())
		{
			static FName CollisionProfileName(TEXT("Ragdoll"));
			GetMesh()->SetCollisionProfileName(CollisionProfileName);
		}
		SetActorEnableCollision(true);

		// disable collisions on capsule
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECR_Ignore);
		
		//
	}
	return Died;
}

FTDAttribute ATDCharacter::GetHealthAttribute_Implementation() const
{
	return Health;
}

bool ATDCharacter::IsAlive_Implementation() const
{
	return Health.CurrentValue > 0;
}

FTDWeaponAttributes ATDCharacter::GetWeaponConfig_Implementation() const
{
	static FTDWeaponAttributes DefaultWeaponData;

	
	return CurrentWeapon ? 
		FTDWeaponAttributes
	{
		CurrentWeapon->GetWeaponConfig(),
		CurrentWeapon->GetCurrentAmmoInClip(),
		CurrentWeapon->GetCurrentAmmo(),
	} : DefaultWeaponData;
}

void ATDCharacter::ScoreKill_Implementation(AActor * KilledActor, int32 Points)
{
	const int32 AppliedScore = GetWorld()->GetAuthGameMode<ADiplomaTDGameMode>()->Killed(this->Controller, KilledActor, Points);
	OnScored.Broadcast(AppliedScore, Execute_GetPoints(this));
}

int32 ATDCharacter::GetPoints_Implementation() const
{
	return GetPlayerState()->Score;
}

void ATDCharacter::SubscribeOnScoreIncreased_Implementation(UObject* Subcriber, FName UFunctionName)
{
	OnScored.AddUFunction(Subcriber, UFunctionName);
}

FTDAttribute ATDCharacter::GetTowerAttribute_Implementation() const
{
	return TowersAttribute;
}

FTDAttribute ATDCharacter::GetBombsAttribute_Implementation() const
{
	return FTDAttribute{ BombSpawnerComponent->GetMaxBombCount(), BombSpawnerComponent->GetCurrentBombCount() };
}

void ATDCharacter::SubscribeOnTowerSpawned_Implementation(UObject* Object, FName FunctionName)
{
	OnTowerSpawned.AddUFunction(Object, FunctionName);
}

void ATDCharacter::SubscribeOnBombSpawnEvent_Implementation(UObject* Object, FName FunctionName)
{
	OnBombSpawnEvent.AddUFunction(Object, FunctionName);
}

float ATDCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
                               AActor* DamageCauser)
{
	float AppliedDamage = 0.f;
	if (Health.CurrentValue > 0.f)
	{
		AppliedDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
		if (AppliedDamage > 0.f)
		{
			Health.CurrentValue -= AppliedDamage;
			if (Health.CurrentValue <= 0.f)
			{
				Die(AppliedDamage, DamageEvent, EventInstigator, DamageCauser);
				//@todo dlavoryk: Die
			}
			else
			{
				//@todo dlavoryk: play hit
			}
			//@todo dlavoryk: make noise
		}
	}
	return AppliedDamage;
}

// Called when the game starts or when spawned
void ATDCharacter::BeginPlay()
{
	Super::BeginPlay();
	SpawnDefaultInventory();
	if (ensureAlways(CurrentWeapon))
	{
		CurrentWeapon->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));
	}
}

void ATDCharacter::OnStartFire()
{
	if (!bIsFiring)
	{
		bIsFiring = true;
		if (CurrentWeapon)
		{
			CurrentWeapon->StartFire();
			OnStartFire_Event.Broadcast();
		}
		else
		{
			OnStopFire_Event.Broadcast();
		}
	}
}

void ATDCharacter::OnStopFire()
{
	if (bIsFiring)
	{
		bIsFiring = false;
		if (CurrentWeapon)
		{
			CurrentWeapon->StopFire();
			OnStopFire_Event.Broadcast();
		}
		else
		{
			OnStopFire_Event.Broadcast();
		}
	}
}

void ATDCharacter::OnStartReloading()
{
	CurrentWeapon->StartReload();
}

void ATDCharacter::OnStartBuildingTower()
{
	if (TowerGhost)
	{
		OnTowerBuilt();
	}
	else
	{
		if (CanBuildTower())
		{
			TowerGhost = GetWorld()->SpawnActor<AStaticMeshActor>(TowerGhostClass);
			GetWorld()->GetTimerManager().SetTimerForNextTick(this, &ATDCharacter::OnTowerBuilding);
		}
	}
}

void ATDCharacter::OnTowerBuilding()
{
	if (TowerGhost)
	{
		ContiniousTowerBuilding(TowerGhost);
		GetWorld()->GetTimerManager().SetTimerForNextTick(this, &ATDCharacter::OnTowerBuilding);
	}
}

void ATDCharacter::OnTowerBuilt()
{
	if (TowerGhost)
	{
		DestroyTowerGhost();
		FinishBuildingTower(TowerGhost);
		OnTowerSpawned.Broadcast();
		TowerGhost = nullptr;
	}
}

void ATDCharacter::OnCancelBuildingTower()
{
	if (TowerGhost)
	{
		DestroyTowerGhost();
		TowerGhost = nullptr;
	}
}

void ATDCharacter::DestroyTowerGhost()
{
	TowerGhost->SetActorEnableCollision(false);
	TowerGhost->SetHidden(true);
	TowerGhost->SetLifeSpan(0.1f);
}

void ATDCharacter::ContiniousTowerBuilding(AStaticMeshActor* Ghost)
{
	const FVector Start= FirstPersonCameraComponent->GetComponentLocation();
	const FVector End = FirstPersonCameraComponent->GetForwardVector() * PlaceTowerRadius + Start;

	TArray<FHitResult> HitResults;
	FCollisionObjectQueryParams ObjectQueryParams;
	ObjectQueryParams.AddObjectTypesToQuery(ECollisionChannel::ECC_WorldStatic);
	FCollisionQueryParams Params = FCollisionQueryParams::DefaultQueryParam;

	const bool bHit = GetWorld()->LineTraceMultiByObjectType(HitResults, Start, End, ObjectQueryParams, Params);

	if (bHit && ensure(HitResults.Num()))
	{
		Ghost->SetActorLocation(HitResults[0].Location);
	} 
}

void ATDCharacter::FinishBuildingTower(AStaticMeshActor* Ghost)
{
	AActor * Tower = UGameplayStatics::BeginDeferredActorSpawnFromClass(GetWorld(), TowerClass, Ghost->GetActorTransform());
	Tower->SetInstigator(this);
	UGameplayStatics::FinishSpawningActor(Tower, Ghost->GetTransform());
	//AActor* Tower = GetWorld()->SpawnActor<AActor>(TowerClass, Ghost->GetActorTransform());
	DecreaseTowerCounter();
}

void ATDCharacter::DecreaseTowerCounter()
{
	--TowersAttribute.CurrentValue;
}

bool ATDCharacter::CanBuildTower() const
{
	return TowersAttribute.CurrentValue > 0;
}

void ATDCharacter::MoveForward(float Value)
{
	if (Value != 0.f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void ATDCharacter::MoveRight(float Value)
{
	if (Value != 0.f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void ATDCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ATDCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ATDCharacter::SpawnDefaultInventory()
{
	for (const TSubclassOf<ATDWeapon> & WeaponClass : DefaultInventoryClasses)
	{
		if (WeaponClass)
		{
			FActorSpawnParameters SpawnInfo;
			SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			ATDWeapon* NewWeapon = GetWorld()->SpawnActor<ATDWeapon>(WeaponClass, SpawnInfo);
			check(NewWeapon);
			//@todo dlavoryk: check if I need to do anything
			NewWeapon->SetInstigator(this);
			NewWeapon->SetOwner(this);
			Inventory.AddUnique(NewWeapon);
		}
	}
	if (ensureAlways(Inventory.Num()))
	{
		CurrentWeapon = Inventory[0];
	}
}

void ATDCharacter::OnBombSpawn()
{
	FVector SpawnerLocation;
	FRotator SpawnerRotation;
	GetActorEyesViewPoint(SpawnerLocation, SpawnerRotation);
	const float DirectionOffset = 200;
	const FVector InitialDirection = DirectionOffset * SpawnerRotation.Vector();
	BombSpawnerComponent->SpawnBomb(SpawnerLocation + InitialDirection, InitialDirection);
	OnBombSpawnEvent.Broadcast();
}




