// Fill out your copyright notice in the Description page of Project Settings.


#include "TDCombatInterface.h"

// Add default functionality here for any ITDCombatInterface functions that are not pure virtual.
float ITDCombatInterface::GetCurrentMeleeAnimMontageLength_DEFAULT(const UObject* This)
{
	return Execute_GetCurrentMeleeAnimMontage(This)->GetPlayLength();
}
