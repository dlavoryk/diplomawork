// Fill out your copyright notice in the Description page of Project Settings.


#include "TDPlayerController.h"
#include "TDAbleToDie.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/HUD.h"
#include "DiplomaTD/UI/DiplomaTDHUD.h"
#include "Character/TDCharacter.h"
#include "DiplomaTD/UI/TDHUDWidgetInterface.h"
#include "TimerManager.h"


ATDPlayerController::ATDPlayerController()
{
	PrimaryActorTick.bCanEverTick = true;
	SetActorTickEnabled(false);
}

bool ATDPlayerController::IsUIRefreshRequested() const
{
	return mUIRefresh_Handle.IsValid();
}

void ATDPlayerController::RequestUIRefresh()
{
	if (!IsUIRefreshRequested())
	{
		RequestUIRefresh_Impl();
	}
}

void ATDPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	const auto MyPawn = GetPawn<ATDCharacter>();
	if (!MyPawn)
	{
		return;
	}
	InputComponent->BindAction("Jump", EInputEvent::IE_Pressed, MyPawn, &ACharacter::Jump);
	InputComponent->BindAction("Jump", EInputEvent::IE_Released, MyPawn, &ACharacter::StopJumping);

	InputComponent->BindAction("Fire", EInputEvent::IE_Pressed, MyPawn, &ATDCharacter::OnStartFire);
	InputComponent->BindAction("Fire", EInputEvent::IE_Released, MyPawn, &ATDCharacter::OnStopFire);

	InputComponent->BindAction("Reload", EInputEvent::IE_Pressed, MyPawn, &ATDCharacter::OnStartReloading);

	InputComponent->BindAction("ThrowBomb", EInputEvent::IE_Pressed, MyPawn, &ATDCharacter::OnBombSpawn);

	InputComponent->BindAxis("MoveForward", MyPawn, &ATDCharacter::MoveForward);
	InputComponent->BindAxis("MoveRight", MyPawn, &ATDCharacter::MoveRight);

	InputComponent->BindAxis("Turn", MyPawn, &APawn::AddControllerYawInput);
	InputComponent->BindAxis("TurnRate", MyPawn, &ATDCharacter::TurnAtRate);
	InputComponent->BindAxis("LookUp", MyPawn, &APawn::AddControllerPitchInput);
	InputComponent->BindAxis("LookUpRate", MyPawn, &ATDCharacter::LookUpAtRate);


	InputComponent->BindAction("BuildTower", EInputEvent::IE_Released, MyPawn, &ATDCharacter::OnStartBuildingTower);
	InputComponent->BindAction("CancelBuildingTower", EInputEvent::IE_Released, MyPawn, &ATDCharacter::OnCancelBuildingTower);
	
}

void ATDPlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	SetupInputComponent();
	ITDShooter::Execute_SubscribeOnScoreIncreased(InPawn, this, GET_FUNCTION_NAME_CHECKED(ATDPlayerController, OnScoreChanged));

	ITDDefenseBuilder::Execute_SubscribeOnBombSpawnEvent(InPawn, this, GET_FUNCTION_NAME_CHECKED(ATDPlayerController, OnBombCountUpdate));
	ITDDefenseBuilder::Execute_SubscribeOnTowerSpawned(InPawn, this, GET_FUNCTION_NAME_CHECKED(ATDPlayerController, OnTowerSpawned));

	InPawn->OnTakeAnyDamage.AddUniqueDynamic(this, &ATDPlayerController::OnTakeAnyDamage);
		
	const auto MyPawn = GetPawn<ATDCharacter>();
	MyPawn->OnStartFire_Event.AddUObject(this, &ATDPlayerController::OnStartFire);
	MyPawn->OnStopFire_Event.AddUObject(this, &ATDPlayerController::OnStopFire);
	
	SetActorTickEnabled(true);

	RequestUIRefresh();
	GetHUD<ADiplomaTDHUD>()->UpdateHealth(ITDAbleToDie::Execute_GetHealthAttribute(GetPawn()));

}

void ATDPlayerController::OnUnPossess()
{
	Super::OnUnPossess();
	SetActorTickEnabled(false);
}

void ATDPlayerController::RequestUIRefresh_Impl()
{
	mUIRefresh_Handle = GetWorld()->GetTimerManager().SetTimerForNextTick(this, &ATDPlayerController::RefreshUI);
}

void ATDPlayerController::RefreshUI()
{
	RefreshUI_Implementation();

	if (bLoopRefresh)
	{
		RequestUIRefresh_Impl();
	}
	else
	{
		mUIRefresh_Handle.Invalidate();
	}
}

void ATDPlayerController::RefreshUI_Implementation()
{
	FTDUIEquipment UIEQuipment
	{
		ITDShooter::Execute_GetWeaponConfig(GetPawn()),
		ITDDefenseBuilder::Execute_GetTowerAttribute(GetPawn()),
		ITDDefenseBuilder::Execute_GetBombsAttribute(GetPawn()),
	};

	GetHUD<ADiplomaTDHUD>()->UpdateEquipment(UIEQuipment);
	GetHUD<ADiplomaTDHUD>()->UpdateHealth(ITDAbleToDie::Execute_GetHealthAttribute(GetPawn()));

}

void ATDPlayerController::OnTakeAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
                                          AController* InstigatedBy, AActor* DamageCauser)
{
	RequestUIRefresh();
}

void ATDPlayerController::OnTowerSpawned()
{
	RequestUIRefresh();
}

void ATDPlayerController::OnBombCountUpdate()
{
	RequestUIRefresh();
}

void ATDPlayerController::OnScoreChanged()
{
	
}

void ATDPlayerController::OnStartFire()
{
	bLoopRefresh = true;
	RequestUIRefresh();
}

void ATDPlayerController::OnStopFire()
{
	bLoopRefresh = false;
}
