// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon/TDWeapon.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "DiplomaTD/Character/DiplomaTDCharacter.h"
#include "GameFramework/PlayerController.h"
#include "AIController.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Projectile/TDProjectile.h"
#include "DrawDebugHelpers.h"
#include "DiplomaTD/DiplomaTD.h"

// @todo dlavoryk: I have to understand all the code that is writeen here and refactor is so it was like My Style

// Sets default values for this component's properties
ATDWeapon::ATDWeapon()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryActorTick.bCanEverTick = true;

	// ...

	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh1P"));
	Mesh1P->VisibilityBasedAnimTickOption = EVisibilityBasedAnimTickOption::OnlyTickPoseWhenRendered;
	Mesh1P->bReceivesDecals = false;
	Mesh1P->CastShadow = false;
	Mesh1P->SetCollisionObjectType(ECC_WorldDynamic);
	Mesh1P->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Mesh1P->SetCollisionResponseToAllChannels(ECR_Ignore);
	RootComponent = Mesh1P;

	
	bIsFireRequested = false;
	bIsReloadRequested = false;
	CurrentAmmo = 0;
	CurrentAmmoInClip = 0;
}

void ATDWeapon::ApplyWeaponConfig(FTDProjectileWeaponData& Data)
{
	Data = ProjectileConfig;
}

void ATDWeapon::StartFire()
{
	if (!bIsFireRequested)
	{
		bIsFireRequested = true;
		DetermineWeaponState();
	}
}

void ATDWeapon::StopFire()
{
	if (bIsFireRequested)
	{
		bIsFireRequested = false;
		DetermineWeaponState();
	}
}

void ATDWeapon::StartReload()
{
	if (CanReload())
	{
		bIsReloadRequested = true;
		DetermineWeaponState();
		// @todo dlavoryk: Start reload animation and get reload time
		static const float AnimDuration = 0.05;

		GetWorld()->GetTimerManager().SetTimer(TimerHandle_StopReload, this, &ATDWeapon::StopReloading, AnimDuration, false);

		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ReloadWeapon, this, &ATDWeapon::ReloadWeapon, FMath::Max(0.1f, AnimDuration - 0.1f), false);

		//@todo dlavoryk: play weapon sound

	}
}

void ATDWeapon::ReloadWeapon()
{
	const int32 ClipDelta = FMath::Min(WeaponConfig.AmmoPerClip - CurrentAmmoInClip, CurrentAmmo - CurrentAmmoInClip);

	if (ClipDelta > 0)
	{
		CurrentAmmoInClip += ClipDelta;
	}
}

void ATDWeapon::StopReloading()
{
	if (mCurrentState == ETDWeaponState::wsReloading)
	{
		bIsReloadRequested = false;
		DetermineWeaponState();
		// @todo dlavoryk: stop weapon animation
	}
}

bool ATDWeapon::CanReload() const
{
	//@todo dlavoryk: check this function
	const bool bCanReload = true;// (!MyPawn || MyPawn->CanReload());
	const bool bGotAmmo = (CurrentAmmoInClip < WeaponConfig.AmmoPerClip) && (CurrentAmmo - CurrentAmmoInClip > 0 || HasInfiniteClip());
	const bool bStateOKToReload = ((mCurrentState == ETDWeaponState::wsIdle) || (mCurrentState == ETDWeaponState::wsFiring));
	return ((bCanReload) && (bGotAmmo) && (bStateOKToReload));
}

bool ATDWeapon::CanFire() const
{
	//@todo dlavoryk: check this function
	const bool bCanFire = true;// MyPawn&& MyPawn->CanFire();
	const bool bStateOKToFire = ((mCurrentState == ETDWeaponState::wsIdle) || (mCurrentState == ETDWeaponState::wsFiring));
	return ((bCanFire) && (bStateOKToFire) && (!bIsReloadRequested));
}

bool ATDWeapon::HasInfiniteClip() const
{
	return WeaponConfig.bInfiniteClip;
}

bool ATDWeapon::HasInfiniteAmmo() const
{
	return WeaponConfig.bInfiniteAmmo;
}

int32 ATDWeapon::GetCurrentAmmo() const
{
	return CurrentAmmo;
}

int32 ATDWeapon::GetCurrentAmmoInClip() const
{
	return CurrentAmmoInClip;
}

void ATDWeapon::PostInitProperties()
{
	Super::PostInitProperties();
	if (WeaponConfig.InitialClips > 0)
	{
		CurrentAmmo = WeaponConfig.InitialClips * WeaponConfig.AmmoPerClip;
		CurrentAmmoInClip = WeaponConfig.AmmoPerClip;
	}
}

void ATDWeapon::HandleFire()
{
	if ((CurrentAmmoInClip > 0 || HasInfiniteAmmo() || HasInfiniteClip()) && CanFire())
	{
		UseAmmo();
		SimulateFire();
		//@todo dlavoryk: add this variable to fix automatic shooting
//		if (bAllowAutomaticWeaponCatchup)
		GetWorldTimerManager().SetTimer(TimerHandle_HandleFiring, this, &ATDWeapon::HandleReFire, FMath::Max<float>(WeaponConfig.TimeBetweenShots + TimerIntervalAdjustment, SMALL_NUMBER), false);
		TimerIntervalAdjustment = 0.f;
	}
	else if (CanReload())
	{
		StartReload();
	}
	else //if (MyPawn && MyPawn->IsLocallyControlled())
	{
		if (GetCurrentAmmo() == 0) // && !bRefiring
		{
			//@todo dlavoryk:
			// Play sound "out of ammo"
			// notify Out Of Ammo
		}
		// if (BoorstCounter > 0)
		{
			OnBurstFinished();
		}
	}

	if (CurrentAmmoInClip <= 0 && CanReload())
	{
		StartReload();
	}
	
	LastFireTime = GetWorld()->GetTimeSeconds();

}

void ATDWeapon::HandleReFire()
{
	UWorld* MyWorld = GetWorld();

	float SlackTimeThisFrame = FMath::Max(0.0f, (MyWorld->TimeSeconds - LastFireTime) - WeaponConfig.TimeBetweenShots);

	if (bAllowAutomaticWeaponCatchup)
	{
		TimerIntervalAdjustment -= SlackTimeThisFrame;
	}
	HandleFire();
}

void ATDWeapon::SimulateFire()
{
	// Show some fire effect, spawn projectile
	FVector ShootDirection = GetAdjustedAim();
	FVector Origin = GetMuzzleLocation();
	// trace from camera to check what's under crosshair
	const float ProjectileAdjustRange = 10000.0f;
	const FVector StartTrace = GetCameraDamageStartLocation(ShootDirection);
	const FVector EndTrace = StartTrace + ShootDirection * ProjectileAdjustRange;

	//DrawDebugDirectionalArrow(GetWorld(), StartTrace, EndTrace, 4, FColor::Red, false, 3.5f, 0.0f, 2.0f);

	
	FHitResult Impact = WeaponTrace(StartTrace, EndTrace);
	if (Impact.bBlockingHit)
	{
		const FVector AdjustedDir = (Impact.ImpactPoint - Origin).GetSafeNormal();
		bool bWeaponPenetration = false;

		const float DirectionDot = FVector::DotProduct(AdjustedDir, ShootDirection);
		if (DirectionDot < 0.0f)
		{
			// shooting backwards = weapon is penetrating
			bWeaponPenetration = true;
		}
		else if (DirectionDot < 0.5f)
		{
			// check for weapon penetration if angle difference is big enough
			// raycast along weapon mesh to check if there's blocking hit

			FVector MuzzleStartTrace = Origin - GetMuzzleDirection() * 150.0f;
			FVector MuzzleEndTrace = Origin;
			FHitResult MuzzleImpact = WeaponTrace(MuzzleStartTrace, MuzzleEndTrace);

			if (MuzzleImpact.bBlockingHit)
			{
				bWeaponPenetration = true;
			}
		}

		if (bWeaponPenetration)
		{
			// spawn at crosshair position
			Origin = Impact.ImpactPoint - ShootDirection * 10.0f;
		}
		else
		{
			// adjust direction to hit
			ShootDirection = AdjustedDir;
		}
	}
	//ServerFireProjectile(Origin, ShootDir);
	SpawnProjectile(Origin, ShootDirection);
	
}

FVector ATDWeapon::GetAdjustedAim() const
{
	FVector Result = FVector::ZeroVector;

	APawn* OwnerPawn = Cast<APawn>(GetOwner());

	//@todo dlavoryk: use TDPlayerController
	APlayerController* const PlayerController = OwnerPawn ? Cast<APlayerController>(OwnerPawn->Controller) : nullptr;//s GetInstigatorController<APlayerController>();
	if (PlayerController)
	{
		FVector CameraLocation;
		FRotator CameraRotation;
		PlayerController->GetPlayerViewPoint(CameraLocation, CameraRotation);
		Result = CameraRotation.Vector();
	}
	else if (OwnerPawn)
	{
		//@todo dlavoryk: check if we are controlled by AIController
		//if (){}
		//else
		{
			Result = GetActorRotation().Vector();
		}
	}
	else
	{
		Result = GetMuzzleDirection();
	}
	return Result;
}

FVector ATDWeapon::GetMuzzleLocation() const
{
	USkeletalMeshComponent* UseMesh = GetWeaponMesh();
	return UseMesh->GetSocketLocation(MuzzleAttachPoint);
}

FVector ATDWeapon::GetMuzzleDirection() const
{
	USkeletalMeshComponent* UseMesh = GetWeaponMesh();
	return UseMesh->GetSocketRotation(MuzzleAttachPoint).Vector();
}

FHitResult ATDWeapon::WeaponTrace(const FVector& TraceFrom, const FVector& TraceTo) const
{

	// Perform trace to retrieve hit info
	FCollisionQueryParams TraceParams(SCENE_QUERY_STAT(WeaponTrace), true, GetInstigator());
	if (GetOwner())
	{
		TraceParams.AddIgnoredActor(GetOwner());
	}
	TraceParams.bReturnPhysicalMaterial = true;

	FHitResult Hit(ForceInit);
	GetWorld()->LineTraceSingleByChannel(Hit, TraceFrom, TraceTo, COLLISION_WEAPON, TraceParams);

	return Hit;
}

FVector ATDWeapon::GetCameraDamageStartLocation(const FVector& AimDir) const
{
	APawn* MyPawn = Cast<APawn>(GetOwner());
	APlayerController* PC = MyPawn ? Cast<APlayerController>(MyPawn->Controller) : nullptr;//= MyPawn ? Cast<APlayerController>(MyPawn->Controller) : nullptr;
	AAIController* AIPC   = MyPawn ? Cast<AAIController>(MyPawn->Controller) : nullptr;//= MyPawn ? Cast<AAIController>(MyPawn->Controller) : nullptr;
	FVector OutStartTrace = FVector::ZeroVector;

	if (PC)
	{
		// use player's camera
		FRotator UnusedRot;
		PC->GetPlayerViewPoint(OutStartTrace, UnusedRot);

		// Adjust trace so there is nothing blocking the ray between the camera and the pawn, and calculate distance from adjusted start
		OutStartTrace = OutStartTrace + AimDir * ((MyPawn->GetActorLocation() - OutStartTrace) | AimDir);
	}
	else if (AIPC)
	{
		OutStartTrace = GetMuzzleLocation();
	}
	else
	{
		OutStartTrace = GetMuzzleLocation() + AimDir * 100.f;
	}

	return OutStartTrace;
}

void ATDWeapon::SpawnProjectile(FVector Location, FVector Direction)
{
	//DrawDebugSphere(GetWorld(), Location, 10.f, 2, FColor::Green, false, 10.f);
	//DrawDebugSphere(GetWorld(), Location + Direction * 100.f, 10.f, 2, FColor::Black, false, 10.f);
	FTransform SpawnTM(Direction.Rotation(), Location);
	ATDProjectile* Projectile = Cast<ATDProjectile>(UGameplayStatics::BeginDeferredActorSpawnFromClass(this, ProjectileConfig.ProjectileClass, SpawnTM));
	if (Projectile)
	{
		Projectile->SetInstigator(GetInstigator());
		Projectile->SetOwner(this);
		Projectile->InitVelocity(Direction);

		UGameplayStatics::FinishSpawningActor(Projectile, SpawnTM);
	}
}

void ATDWeapon::OnBurstStarted()
{
	// start firing, can be delayed to satisfy TimeBetweenShots
	const float GameTime = GetWorld()->GetTimeSeconds();
	if (LastFireTime > 0 && WeaponConfig.TimeBetweenShots > 0.0f &&
		LastFireTime + WeaponConfig.TimeBetweenShots > GameTime)
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_HandleFiring, this, &ATDWeapon::HandleFire, LastFireTime + WeaponConfig.TimeBetweenShots - GameTime, true);
	}
	else
	{
		HandleFire();
	}
}

void ATDWeapon::OnBurstFinished()
{
	//// stop firing FX on remote clients
	//BurstCounter = 0;

	//// stop firing FX locally, unless it's a dedicated server
	////if (GetNetMode() != NM_DedicatedServer)
	////{
	//StopSimulatingWeaponFire();
	////}

	GetWorldTimerManager().ClearTimer(TimerHandle_HandleFiring);
	//bRefiring = false;

	//// reset firing interval adjustment
	TimerIntervalAdjustment = 0.0f;
}

void ATDWeapon::UseAmmo()
{
	if (!HasInfiniteAmmo())
	{
		CurrentAmmoInClip--;
	}

	if (!HasInfiniteAmmo() && !HasInfiniteClip())
	{
		CurrentAmmo--;
	}

	//@todo dlavoryk: 
	// if bot -- check ammo
}

void ATDWeapon::SetWeaponState(ETDWeaponState NewState)
{
	const ETDWeaponState PrevState = mCurrentState;

	if (PrevState == ETDWeaponState::wsFiring && NewState != ETDWeaponState::wsFiring)
	{
		// finish firing
		OnBurstFinished();
	}

	//@todo dlavoryk: 
	mCurrentState = NewState;

	if (PrevState != ETDWeaponState::wsFiring && NewState == ETDWeaponState::wsFiring)
	{
		// Start firing
		OnBurstStarted();
	}

}

ETDWeaponState ATDWeapon::GetCurrentState() const
{
	return mCurrentState;
}

void ATDWeapon::DetermineWeaponState()
{
	ETDWeaponState NewState = ETDWeaponState::wsIdle;
	if (bIsReloadRequested)
	{
		if (CanReload())
		{
			NewState = ETDWeaponState::wsReloading;
		}
		else
		{
			NewState = mCurrentState;
		}
	}
	else if (bIsFireRequested && CanFire())
	{
		NewState = ETDWeaponState::wsFiring;
	}

	SetWeaponState(NewState);
}

USkeletalMeshComponent* ATDWeapon::GetWeaponMesh() const
{
	return Mesh1P;
}

