// Fill out your copyright notice in the Description page of Project Settings.


#include "Buildings/Tower/TDTower.h"
#include "Engine/World.h"
#include "EngineUtils.h"
#include "Weapon/TDWeapon.h"
#include "Character/NPC/NPC.h"

// Sets default values
ATDTower::ATDTower()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
}

void ATDTower::Init(ATDWeapon* Weapon, UStaticMeshComponent* WeaponHorizontalOrigin)
{
	MyWeapon = Weapon;
	MyWeaponHorizontalOrigin = WeaponHorizontalOrigin;
}

// Called when the game starts or when spawned
void ATDTower::BeginPlay()
{
	Super::BeginPlay();
	WeaponPostInit();
}

float ATDTower::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
	AActor* DamageCauser)
{
	float AppliedDmg = 0.f;
	if (Health.CurrentValue > 0.f)
	{
		AppliedDmg = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
		if (AppliedDmg > 0.f)
		{
			Health.CurrentValue -= AppliedDmg;
			if (Health.CurrentValue <= 0.f)
			{
				Die();
			}
		}
	}
	return AppliedDmg;
}

FTDAttribute ATDTower::GetHealthAttribute_Implementation() const
{
	return Health;
}

bool ATDTower::IsAlive_Implementation() const
{
	return Health.CurrentValue > 0;
}

void ATDTower::Die_Implementation()
{
	Health.CurrentValue = 0.f;
}

// Called every frame
void ATDTower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (Target && ITDAbleToDie::Execute_IsAlive(Target))
	{
		const FVector MuzzleDir = MyWeapon->GetMuzzleDirection();
		const FVector VecToTarget = Target->GetActorLocation() - MyWeapon->GetMuzzleLocation();
		const float Rotation = GetAngleInDegreesBetweenDirections(MuzzleDir, VecToTarget.GetSafeNormal());
		MyWeaponHorizontalOrigin->AddRelativeRotation(FRotator(0, Rotation, 0));
		
		if (HasLOSToActorWithDistanceCheck(Target))
		{
			mWaitingTime = 0.f;
			MyWeapon->StartFire();
		}
		else
		{
			mWaitingTime += DeltaTime;
			if (mWaitingTime > MaxWaitingForEnemyTime)
			{
				MyWeapon->StopFire();
				// Target will be updated on next tick
				SetTarget(nullptr);
			}
		}
	}
	else
	{
		MyWeapon->StopFire();
 
		SetTarget(FindNearestTarget());
	}
}


ANPC* ATDTower::FindNearestTarget() const
{
	const float SquaredR = AttackRadius * AttackRadius;
	ANPC* FutureTarget = nullptr;
	const FVector WeaponLocation = MyWeapon->GetActorLocation();

	for (TActorIterator<ANPC> It(GetWorld()); It; ++It)
	{
		if (!ITDAbleToDie::Execute_IsAlive(*It))
		{
			continue;;
		}
		const float DistToPotentialTargetSquared = FVector::DistSquared(It->GetActorLocation(), WeaponLocation);
		if (DistToPotentialTargetSquared < SquaredR)
		{
			FCollisionQueryParams CollisionParams(SCENE_QUERY_STAT(LineOfSight), true, this);
			CollisionParams.AddIgnoredActor(MyWeapon);
			CollisionParams.AddIgnoredActor(*It);

			const bool bIsHit = GetWorld()->LineTraceTestByChannel(WeaponLocation, It->GetActorLocation(), ECollisionChannel::ECC_Visibility, CollisionParams);
			// if nothing prevent from firing an enemy
			if (!bIsHit)
			{
				if (!FutureTarget)
				{
					FutureTarget = *It;
				}
				else
				{// check if New target is the nearest
					if (FVector::DistSquared(FutureTarget->GetActorLocation(), WeaponLocation) > DistToPotentialTargetSquared)
					{
						FutureTarget = *It;
					}
				}
			}
		}
		
	}
	
	return FutureTarget;
}

bool ATDTower::HasLOSToActorWithDistanceCheck(ANPC* ActorToCheck) const
{
	bool bHasLOS = false;
	
	const float SquaredR = AttackRadius * AttackRadius;
	const FVector WeaponLocation = MyWeapon->GetActorLocation();
	const float DistToPotentialTargetSquared = FVector::DistSquared(ActorToCheck->GetActorLocation(), WeaponLocation);
	if (DistToPotentialTargetSquared < SquaredR)
	{
		bHasLOS = HasLOSToActorWithoutDistanceCheck(ActorToCheck);
	}
	return bHasLOS;
}

bool ATDTower::HasLOSToActorWithoutDistanceCheck(ANPC* ActorToCheck) const
{
	FCollisionQueryParams CollisionParams(SCENE_QUERY_STAT(LineOfSight), true, this);
	CollisionParams.AddIgnoredActor(MyWeapon);
	CollisionParams.AddIgnoredActor(ActorToCheck);

	const bool bIsHit = GetWorld()->LineTraceTestByChannel(MyWeapon->GetActorLocation(), ActorToCheck->GetActorLocation(), ECollisionChannel::ECC_Visibility, CollisionParams);
	return !bIsHit;
}

void ATDTower::SetTarget(ANPC* FutureTarget)
{
	mWaitingTime = 0.f;
	ResetCurrentTarget();
	if (FutureTarget && ITDAbleToDie::Execute_IsAlive(FutureTarget))
	{
		SetTarget_Internal(FutureTarget);
	}
}

void ATDTower::SetTarget_Internal(ANPC* FutureTarget)
{
	Target = FutureTarget;
	Target->OnEndPlay.AddUniqueDynamic(this, &ATDTower::OnTargetDie);
}

void ATDTower::ResetCurrentTarget()
{
	if (Target)
	{
		Target->OnEndPlay.RemoveAll(this);
		Target = nullptr;
	}
}

void ATDTower::WeaponPostInit()
{
	MyWeapon->SetInstigator(GetInstigator());
	MyWeapon->SetOwner(this);
}

void ATDTower::OnTargetDie(AActor* Actor, EEndPlayReason::Type Reason)
{
	check(Target == Actor);
	ResetCurrentTarget();
}
//CurDirection, DesiredDirection
float ATDTower::GetAngleInDegreesBetweenDirections(const FVector& Direction1, const FVector& Direction2)
{
	const float Atan2Dir2 = atan2(Direction2.Y, Direction2.X);
	const float Atan2Dir1 = atan2(Direction1.Y, Direction1.X);
	float ResultAngle = ResultAngle = FMath::RadiansToDegrees(Atan2Dir2 - Atan2Dir1);
	return ResultAngle;
}

