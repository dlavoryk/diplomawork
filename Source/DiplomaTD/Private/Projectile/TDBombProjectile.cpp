// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile/TDBombProjectile.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Projectile/TDExplosionEffect.h"

void ATDBombProjectile::PostInitializeComponents()
{
	AActor::PostInitializeComponents();

	//CollisionComp->MoveIgnoreActors.Add(GetInstigator());

	SetLifeSpan(WeaponConfig.ProjectileLife);
	MyController = GetInstigatorController();
}

void ATDBombProjectile::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	BombExplode();
	Super::EndPlay(EndPlayReason);

}

void ATDBombProjectile::BombExplode()
{
	if (GetParticleComp())
	{
		UParticleSystemComponent* PointerOnParticleComp = GetParticleComp();
		PointerOnParticleComp->Deactivate();
	}

	// effects and damage origin shouldn't be placed inside mesh at impact point
	const FVector NudgedImpactLocation = GetActorLocation();

	if (WeaponConfig.ExplosionDamage > 0 && WeaponConfig.ExplosionRadius > 0 && WeaponConfig.DamageType)
	{
		ApplyRadialDamage(this, WeaponConfig.ExplosionDamage, NudgedImpactLocation, WeaponConfig.ExplosionRadius, WeaponConfig.DamageType, TArray<AActor*>(), this, MyController.Get());
		//@todo dlavoryk: Call "UAISense_Damage::ReportDamageEvent();
	}

	if (ExplosionTemplate)
	{
		FTransform const SpawnTransform(GetActorRotation(), NudgedImpactLocation);
		ATDExplosionEffect* const EffectActor = GetWorld()->SpawnActorDeferred<ATDExplosionEffect>(ExplosionTemplate, SpawnTransform);
		if (EffectActor)
		{
			//EffectActor->SurfaceHit = Impact;
			UGameplayStatics::FinishSpawningActor(EffectActor, SpawnTransform);
		}
	}
}
