// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/NPCAIController.h"
#include "Perception/AIPerceptionComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "TimerManager.h"
#include "TDAbleToDie.h"
#include "NavigationSystem.h"
#include "Character/NPC/NPC.h"

ANPCAIController::ANPCAIController()
{
	AIPerceptionComponent = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("AIPerceptionComponent"));
	BrainComponent = BT   = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorComp"));
	BlackboardComp        = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackBoardComp"));

}

AActor* ANPCAIController::GetEnemy() const
{
	check(GetBlackboardComponent());
	return GetBlackboardComponent() ? 
		Cast<AActor>(GetBlackboardComponent()->GetValueAsObject(EnemyActor_Name)) :
		nullptr;
}

void ANPCAIController::SetEnemy(AActor* Enemy)
{
	if (GetBlackboardComponent())
	{
		GetBlackboardComponent()->SetValueAsObject(EnemyActor_Name, Enemy);
	}
}

bool ANPCAIController::GetNextPatrolPoint(const FVector& Origin, FVector& RandomLocation, float Radius,
	ANavigationData* NavData, TSubclassOf<UNavigationQueryFilter> FilterClass)
{
	return UNavigationSystemV1::K2_GetRandomReachablePointInRadius(this, Origin, RandomLocation, Radius, NavData, FilterClass);
}

bool ANPCAIController::CanBite() const
{
	return BlackboardComp->GetValueAsBool(TEXT("IsEnemyInMeleeRange"));
}

void ANPCAIController::AttackEnemy()
{
	ANPC* Me = Cast<ANPC>(GetPawn());
	if (!ensure(Me))
	{
		return;
	}

	AActor* Enemy = GetEnemy();

	bool bCanBeAttacked = false;
	
	if (Enemy)
	{
		const bool bCanEverDie = (Cast<ITDAbleToDie>(Enemy) || Enemy->Implements<UTDAbleToDie>());
		if (bCanEverDie && ITDAbleToDie::Execute_IsAlive(Enemy))
		{
			if (LineOfSightTo(Enemy))
			{
				bCanBeAttacked = true;
			}
		}
		else
		{
			//@todo dlavoryk: what is the point of attacking an enemy who cannot die?
			//check(false);
		}
		
	}

	
	if (bCanBeAttacked)
	{
		ITDCombatInterface::Execute_MeleeAttack(Me);
//		Me->MeleeAttack();
	}
	else
	{
		
	}
	
}

void ANPCAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	check(BT);
	const auto Bot = Cast<ANPC>(InPawn);
	if (Bot && Bot->BotBehavior)
	{
		if (Bot->BotBehavior->BlackboardAsset)
		{
			BlackboardComp->InitializeBlackboard(*Bot->BotBehavior->BlackboardAsset);
		}
		BT->StartTree(*(Bot->BotBehavior));
		RunBehaviorTree(Bot->BotBehavior);
	}
}

void ANPCAIController::BeginPlay()
{
	Super::BeginPlay();
	BrainComponent = BT;
	//AIPerceptionComponent->OnTargetPerceptionUpdated.AddUniqueDynamic(this, &ANPCAIController::OnTargetPerceptionUpdated);
	AIPerceptionComponent->OnPerceptionUpdated.AddUniqueDynamic(this, &ANPCAIController::OnPerceptionUpdated_FromVideo);
}

void ANPCAIController::GameHasEnded(AActor* EndGameFocus, bool bIsWinner)
{
	BT->StopTree();
	StopMovement();
	SetEnemy(nullptr);
}

void ANPCAIController::OnPerceptionUpdated(const TArray<AActor*>& UpdatedActors)
{
	for (AActor* El : UpdatedActors)
	{
		if (!El->ActorHasTag(PlayerTag_Name))
		{
			continue;
		}
		if (const FActorPerceptionInfo* Info = AIPerceptionComponent->GetActorInfo(*El))
		{
			int32 i = 0;
			for (; i < Info->LastSensedStimuli.Num(); ++i)
			{
				const FAIStimulus& Stimulus = Info->LastSensedStimuli[i];
				if (Stimulus.WasSuccessfullySensed())
				{
					PlayerHandle.Invalidate();
					switch (i)
					{
						/** Sight config */
					case 0:
						UpdateBlackboardPlayerData(El, true);
						break;
					/** Hearing config? sense? */
					case 1:
						GetBlackboardComponent()->SetValueAsBool(IsInvestigating_Name, Stimulus.WasSuccessfullySensed());
						GetBlackboardComponent()->SetValueAsVector(Destination_Name, Stimulus.StimulusLocation);
						break;;
					default:
						check(false);
						break;
					}
				}
			}
			/** Did not find successfully sensed */
			if (i == Info->LastSensedStimuli.Num())
			{
				GetWorld()->GetTimerManager().SetTimer(PlayerHandle, this, &ANPCAIController::ResetBlackboardPlayerData, 1.f, false, 0.f);
			}
		}
		break;;
	}
}

void ANPCAIController::OnPerceptionUpdated_FromVideo(const TArray<AActor*>& UpdatedActors)
{
	for (AActor* El : UpdatedActors)
	{
		if (!El->ActorHasTag(PlayerTag_Name))
		{
			continue;
		}
		if (const FActorPerceptionInfo* Info = AIPerceptionComponent->GetActorInfo(*El))
		{
			for (int32 i = 0; i < Info->LastSensedStimuli.Num(); ++i)
			{
				const FAIStimulus& Stimulus = Info->LastSensedStimuli[i];
				if (!Stimulus.WasSuccessfullySensed())
				{
					continue;
				}
				switch (i)
				{
					/** Sight config */
				case 0:
					UpdateBlackboardPlayerData(El, Stimulus.WasSuccessfullySensed());
					break;
					/** Hearing config? sense? */
				case 1:
					GetBlackboardComponent()->SetValueAsBool(IsInvestigating_Name, Stimulus.WasSuccessfullySensed());
					GetBlackboardComponent()->SetValueAsVector(Destination_Name, Stimulus.StimulusLocation);
					break;;
				default:
					check(false);
					break;
				}
			}
		}
		break;;
	}
}

void ANPCAIController::UpdateBlackboardPlayerData(AActor* Actor, bool bHasLineOfSight)
{
	if (ensure(GetBlackboardComponent()))
	{
		GetBlackboardComponent()->SetValueAsBool(HasLineOfSight_Name, bHasLineOfSight);
		GetBlackboardComponent()->SetValueAsObject(EnemyActor_Name, Actor);
	}
}

void ANPCAIController::ResetBlackboardPlayerData()
{
	UpdateBlackboardPlayerData(nullptr, false);
}
