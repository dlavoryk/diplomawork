// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/Nodes/TDBTTask_FindPointNearEnemy.h"
#include "AI/NPCAIController.h"
#include "NavigationSystem.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Vector.h"

EBTNodeResult::Type UTDBTTask_FindPointNearEnemy::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	ANPCAIController* MyController = Cast<ANPCAIController>(OwnerComp.GetAIOwner());
	if (!MyController)
	{
		return EBTNodeResult::Failed;
	}

	APawn* MyBody = MyController->GetPawn();

	AActor* Enemy = MyController->GetEnemy();

	if (Enemy && MyBody)
	{
//		const float SearchRadius = 200.f;
		const FVector SearchOrigin = Enemy->GetActorLocation() + DistanceFromEnemy * (MyBody->GetActorLocation() - Enemy->GetActorLocation()).GetSafeNormal();
		FVector Location = FVector::ZeroVector;
		if (UNavigationSystemV1::K2_GetRandomReachablePointInRadius(MyController, SearchOrigin, Location, SearchRadius))
		{
			OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Vector>(BlackboardKey.GetSelectedKeyID(), Location);
			return EBTNodeResult::Succeeded;
		}
	}

	return EBTNodeResult::Failed;
	
}
