// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDWaveStruct.h"
#include "Engine/DataTable.h"
#include "TDWavesStruct.generated.h"
/**
 * 
 */
USTRUCT(BlueprintType)
struct DIPLOMATD_API FTDWavesStruct : public FTableRowBase
{
	GENERATED_BODY()
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite) TArray<FTDWaveStruct> Waves;

	
};
