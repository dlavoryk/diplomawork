// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "DiplomaTDGameMode.h"
#include "UObject/ConstructorHelpers.h"
#include "DiplomaTD/UI/DiplomaTDHUD.h"
#include <TDShooter.h>
#include "GameFramework/PlayerState.h"
#include "TDAbleToDie.h"

ADiplomaTDGameMode::ADiplomaTDGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ADiplomaTDHUD::StaticClass();
}

bool ADiplomaTDGameMode::ReadyToEndMatch_Implementation()
{
	//UE_LOG(LogTemp, Warning, TEXT("%s"), AllEnemiesAreKilled() ? TEXT("ALL KILLED") : TEXT("-"));
	//UE_LOG(LogTemp, Warning, TEXT("%s"), IsMainPlayerAlive() ? TEXT("Alive") : TEXT("RIP"));
	return AllEnemiesAreKilled() || !IsMainPlayerAlive() || IsCheckpointLost();
}

void ADiplomaTDGameMode::HandleMatchHasEnded()
{
	bIsGameWon = IsMainPlayerAlive() && !IsCheckpointLost();
	Super::HandleMatchHasEnded();
}

bool ADiplomaTDGameMode::IsMainPlayerAlive() const
{
	auto FPC = GetWorld()->GetFirstPlayerController();
	return  FPC && FPC->GetPawn() && (!Cast<ITDAbleToDie>(FPC->GetPawn()) || ITDAbleToDie::Execute_IsAlive(FPC->GetPawn()));
}

int32 ADiplomaTDGameMode::GetTotalKilledEnemies_Implementation() const
{
	return 0;
}

bool ADiplomaTDGameMode::IsGameWon() const
{
	return bIsGameWon;
}

int32 ADiplomaTDGameMode::Killed(AController* Killer, AActor* Killed, int32 Score)
{
	check(Killer);
	check(Killer->GetPawn());
	check(Killer->PlayerState);

	Killer->PlayerState->Score += Score;

	//@todo dlavoryk: add some modifiers, etc
	
	if (Killer->GetPawn()->Implements<UTDShooter>() || Cast<ITDShooter>(Killer->GetPawn()))
	{
		
	}
	CheckStatus(Killed);
	return Score;
}

bool ADiplomaTDGameMode::IsCheckpointLost_Implementation() const
{
	return false;
}

void ADiplomaTDGameMode::StartNewWave_Implementation()
{
	check(false);
}

bool ADiplomaTDGameMode::GetNextWave_Implementation(FTDWaveStruct& OutWave)
{
	return false;
}

void ADiplomaTDGameMode::CheckStatus_Implementation(AActor* KilledActor)
{
	check(false);
}

bool ADiplomaTDGameMode::AllEnemiesAreKilled_Implementation() const
{
	return false;
}
