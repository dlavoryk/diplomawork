// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GameFramework/GameMode.h"
#include "TDWaveStruct.h"
#include "DiplomaTDGameMode.generated.h"


//@todo dlavoryk: add "team" to player state to all the pawns.

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FGMEvent);

UCLASS(minimalapi)
class ADiplomaTDGameMode : public AGameMode
{
	GENERATED_BODY()

public:

	
	ADiplomaTDGameMode();

	virtual bool ReadyToEndMatch_Implementation() override;

	virtual void HandleMatchHasEnded() override;
	
	bool IsMainPlayerAlive() const;



	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	int32 GetTotalKilledEnemies() const;

	bool IsGameWon() const;
	
	virtual int32 Killed(AController* Killer, AActor * Killed, int32 Score);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	bool AllEnemiesAreKilled() const;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	bool IsCheckpointLost() const;
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void CheckStatus(AActor * KilledActor);
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	bool GetNextWave(FTDWaveStruct & OutWave);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void StartNewWave();

public:
	
	UPROPERTY(BlueprintAssignable, BlueprintCallable)
	FGMEvent OnWaveFinished;

	
	bool bIsGameWon = false;
};



