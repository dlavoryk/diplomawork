// Fill out your copyright notice in the Description page of Project Settings.


#include "TDGameState.h"
#include "Engine/World.h"
#include "DiplomaTD/UI/DiplomaTDHUD.h"
#include "DiplomaTD/Core/TDEndGameModel.h"
#include "DiplomaTDGameMode.h"
#include "GameFramework/PlayerState.h"
#include "Kismet/GameplayStatics.h"
#include "AI/NPCAIController.h"
#include "BrainComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Character/NPC/NPC.h"


ADiplomaTDGameMode* ATDGameState::GetGM() const
{
	return GetWorld()->GetAuthGameMode<ADiplomaTDGameMode>();
}

void ATDGameState::HandleMatchHasStarted()
{
	Super::HandleMatchHasStarted();
	GetWorld()->GetFirstPlayerController()->SetInputMode(FInputModeGameOnly{});
}

void ATDGameState::HandleMatchHasEnded()
{
	Super::HandleMatchHasEnded();

	APlayerController* FPC = GetWorld()->GetFirstPlayerController();
	FPC->DisableInput(FPC);
	const auto C = FPC->GetPawn<ACharacter>();
	if (GetGM()->IsMainPlayerAlive() &&  ensure(C))
	{
		check(C->GetCharacterMovement());
		C->GetCharacterMovement()->DisableMovement();
	}

	const ADiplomaTDGameMode * AuthGM = GetGM();
	
	FTDEndGameModel EndGameModel;
	EndGameModel.TotalKills  = AuthGM->GetTotalKilledEnemies();
	EndGameModel.TotalPoints = FPC->GetPlayerState<APlayerState>()->Score;
	EndGameModel.bIsWin      = AuthGM->IsGameWon();

	FInputModeUIOnly InputModeUI;
	InputModeUI.SetLockMouseToViewportBehavior(EMouseLockMode::LockAlways);

	FPC->GetHUD<ADiplomaTDHUD>()->HandleMatchFinished(EndGameModel);
	
	GetWorld()->GetFirstPlayerController()->SetInputMode(FInputModeUIOnly{});
	GetWorld()->GetFirstPlayerController()->bShowMouseCursor = true;
	TArray<AActor*> FoundActors;
	
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ANPC::StaticClass(), FoundActors);
	for (auto El : FoundActors)
	{
		const auto MyEl = Cast<ANPC>(El);
		if (const auto Controller = MyEl->GetController<ANPCAIController>())
		{
			if (Controller->GetBrainComponent() && Controller->GetBrainComponent()->IsValidLowLevelFast())
			{
				Controller->GetBrainComponent()->StopLogic(TEXT("Game Is Finished"));
			}
		}
	}	
}