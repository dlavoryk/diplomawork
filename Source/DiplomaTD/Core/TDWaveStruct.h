#pragma once
#include "CoreMinimal.h"
#include "TDWaveEnemy.h"
#include "Engine/DataTable.h"

#include "TDWaveStruct.generated.h"



USTRUCT(BlueprintType)
struct DIPLOMATD_API FTDEnemyCountPair
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite) FTDWaveEnemy WaveEnemy;
	UPROPERTY(EditAnywhere, BlueprintReadWrite) int32 Count = 1;
	
	
public:
	
};



/*
 * 
 */
USTRUCT(BlueprintType)
struct DIPLOMATD_API FTDWaveStruct : public FTableRowBase
{
	GENERATED_BODY()
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite) TArray<FTDEnemyCountPair> WaveEnemies;
	UPROPERTY(EditAnywhere, BlueprintReadWrite) int32 WaveTime = 60;
	UPROPERTY(EditAnywhere, BlueprintReadWrite) int32 WavePassedScore = 10;
	


};
