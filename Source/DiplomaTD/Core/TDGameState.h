// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "TDGameState.generated.h"

struct FTDWaveStruct;
/**
 * 
 */
UCLASS()
class DIPLOMATD_API ATDGameState : public AGameState
{
	GENERATED_BODY()

	

public:

	class ADiplomaTDGameMode* GetGM() const;

protected:
	virtual void HandleMatchHasStarted() override;
	virtual void HandleMatchHasEnded() override;

};
