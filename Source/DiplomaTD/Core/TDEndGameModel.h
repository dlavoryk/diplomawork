#pragma once
#include "CoreMinimal.h"
#include "TDEndGameModel.generated.h"


USTRUCT(BlueprintType)
struct FTDEndGameModel
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite) bool bIsWin;
	UPROPERTY(EditAnywhere, BlueprintReadWrite) int32 TotalPoints;
	UPROPERTY(EditAnywhere, BlueprintReadWrite) int32 TotalKills;

};