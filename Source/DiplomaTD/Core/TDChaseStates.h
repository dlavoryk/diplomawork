#pragma once
#include "CoreMinimal.h"

#include "TDChaseStates.generated.h"


UENUM(BlueprintType)
enum class ETDChaseStates : uint8 
{
	cs_Patrolling  UMETA(DisplayName = "Patrolling"),
	cs_Searching   UMETA(DisplayName = "Searching" ),
	cs_Chasing     UMETA(DisplayName = "Chasing"   ),
};
