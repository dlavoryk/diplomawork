// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "TDGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class DIPLOMATD_API UTDGameInstance : public UGameInstance
{
	GENERATED_BODY()


public:
	UFUNCTION(BlueprintCallable) void LoadMainMenu();
	UFUNCTION(BlueprintCallable) void LoadNextLevel();
	UFUNCTION(BlueprintCallable) void RestartCurrentLevel();
	
protected:
	UPROPERTY(EditAnywhere) FName MainMenuName;
	UPROPERTY(EditAnywhere) FName GameLevelName;

};
