// Fill out your copyright notice in the Description page of Project Settings.


#include "TDGameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"

void UTDGameInstance::LoadMainMenu()
{
	UGameplayStatics::OpenLevel(this, MainMenuName);
}

void UTDGameInstance::LoadNextLevel()
{
	UGameplayStatics::OpenLevel(this, GameLevelName);
}

void UTDGameInstance::RestartCurrentLevel()
{
	UGameplayStatics::OpenLevel(this, GameLevelName);
}
