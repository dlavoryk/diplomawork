// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Templates/SubclassOf.h"
#include "TDWaveEnemy.generated.h"


/**
 * 
 */
USTRUCT(BlueprintType)
struct DIPLOMATD_API FTDWaveEnemy
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<class ANPC> EnemyClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float DefaultHealthCoefficient = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float DefaultDamageCoefficient = 1;
	

	
};
