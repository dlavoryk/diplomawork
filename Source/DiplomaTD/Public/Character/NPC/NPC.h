// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TDAbleToDie.h"
#include "TDCombatInterface.h"
#include "NPC.generated.h"

UCLASS()
class DIPLOMATD_API ANPC : public ACharacter, public ITDAbleToDie, public ITDCombatInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ANPC();


	// Start of ITDAbleToDie Interface
	virtual FTDAttribute GetHealthAttribute_Implementation() const override;
	virtual bool IsAlive_Implementation() const override;
	virtual int32 GetPointsForMyHead_Implementation() const override;
	// End of ITDAbleToDie Interface

	// Start of ITDCombatInterface interface
	virtual void MeleeAttack_Implementation() override;
	virtual UAnimMontage* GetCurrentMeleeAnimMontage_Implementation() const override;
	virtual float GetCurrentMeleeAnimMontageLength_Implementation() const override;
	virtual float GetMeleeDamage_Implementation() const override;
	// End of ITDCombatInterface interface

	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void ModifyHealth(float Coefficient);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void ModifyDamage(float Coefficient);

	
	
protected:
	virtual void BeginPlay() override;
	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	virtual void Die(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser);
	

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void Attack();
	
	
public:
	UPROPERTY(EditAnywhere, Category = Behavior)
	class UBehaviorTree* BotBehavior;
	
protected:


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Health)
	FTDAttribute Health;
	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Score)
	int32 ScoreForMyHead = 1;;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	float MeleeDamage = 20.f;

	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animation)
	UAnimMontage * DefaultMeleeMontage;

	
};
