#pragma once
#include "CoreMinimal.h"
#include "TDCentaurWeaponState.generated.h"

UENUM(BlueprintType)
enum class ETDCentaurWeaponState : uint8
{
	cwsNormal       UMETA(DisplayName = "Normal"),
	cwsSword        UMETA(DisplayName = "Sword"),
	cwsBow          UMETA(DisplayName = "Bow"),
	cwsBowNotAiming UMETA(DisplayName = "BowNotAiming"),
	cwsBowAiming    UMETA(DisplayName = "BowAiming"),


	bbmMAX UMETA(Hidden),
	//bbmNone               UMETA(DisplayName = "None. Simplifies set up"),

};