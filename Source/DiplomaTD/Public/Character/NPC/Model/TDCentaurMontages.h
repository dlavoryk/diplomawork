// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "TDCentaurWeaponState.h"
#include "TDCentaurMontages.generated.h"


USTRUCT(BlueprintType)
struct FTDWeaponTransition
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ETDCentaurWeaponState From;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ETDCentaurWeaponState To;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UAnimMontage* Montage;
};

/**
 * 
 */
UCLASS()
class DIPLOMATD_API UTDCentaurMontages : public UDataAsset
{
	GENERATED_BODY()

public:

	const FTDWeaponTransition* FindTransition(ETDCentaurWeaponState From, ETDCentaurWeaponState To) const;

	
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UAnimMontage* ShootArrowMontage;


	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FTDWeaponTransition> WeaponStateToMontage;

};
