// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "NPC.h"
#include "TDBarghest.generated.h"

UENUM(BlueprintType)
enum class ETDBarghestIdleState : uint8
{
	bsIdleBreath     UMETA(DisplayName = "Idle Breath"),
	bsIdleAggressive UMETA(DisplayName = "Idle Aggressive"),
	bsIdleLookAround UMETA(DisplayName = "Idle Look Around"),

	bs_MAX UMETA(Hidden)
};


UENUM(BlueprintType)
enum class ETDBarghestBiteMontage : uint8
{
	bbmIdleBite           UMETA(DisplayName = "Idle Bite"),
	bbmIdleBiteAggressive UMETA(DisplayName = "Idle Bite Aggressive"),
	bbmRunBite            UMETA(DisplayName = "Run Bite"),
	bbmJumpBite           UMETA(DisplayName = "Jump Bite"),
	bbmJumpBiteAggressive UMETA(DisplayName = "Jump Bite Aggressive"),


	bbmMAX UMETA(Hidden),
	//bbmNone               UMETA(DisplayName = "None. Simplifies set up"),

};

/**
 * 
 */
UCLASS()
class DIPLOMATD_API ATDBarghest : public ANPC
{
	GENERATED_BODY()



	virtual void BeginPlay() override;

	

	UFUNCTION(BlueprintCallable)
	bool CanBite() const;

	UFUNCTION(BlueprintCallable)
	bool IsAggressive() const;

	UFUNCTION(BlueprintCallable)
	void SetIsAggressive(bool IN_bIsAggressive);

	
	UFUNCTION(BlueprintCallable)
	ETDBarghestIdleState GetLastIdleState() const;

	bool HasCriticalHP() const;
	

	virtual UAnimMontage* GetCurrentMeleeAnimMontage_Implementation() const override;

	ETDBarghestBiteMontage GetDesiredMontage() const;

protected:
	UFUNCTION()
	void OnTakeAnyDamage_Impl(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);
	
protected:
	//@todo dlavoryk: it's better to move this Map into separate struct or asset
	UPROPERTY(EditDefaultsOnly, Category = Animation)
	TMap<ETDBarghestBiteMontage, UAnimMontage*> MeleeAttackMontage;

	bool bIsAggressive = false;
};
