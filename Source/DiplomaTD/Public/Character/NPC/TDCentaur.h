// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "NPC.h"
#include "Model/TDCentaurWeaponState.h"
#include "Model/TDCentaurMontages.h"
#include "TDCentaur.generated.h"


/**
 * @todo dlavoryk: consider moving static meshes into some struct
 */
UCLASS()
class DIPLOMATD_API ATDCentaur : public ANPC
{
	GENERATED_BODY()


public:

	ATDCentaur();
	
	UFUNCTION(BlueprintCallable)
	bool IsAiming() const { return GetCentaurWeaponState() == ETDCentaurWeaponState::cwsBowAiming; }

	UFUNCTION(BlueprintCallable)
	bool IsBowActive() const { return GetCentaurWeaponState() == ETDCentaurWeaponState::cwsBowNotAiming || IsAiming(); }

	UFUNCTION(BlueprintCallable)
	void ToggleAim(bool IsAiming);
	
	UFUNCTION(BlueprintCallable)
	void UseBow();

	UFUNCTION(BlueprintCallable)
	void SetIsBowActive(bool bIsActive);
	
	UFUNCTION(BlueprintCallable)
	void Shoot();


	UFUNCTION(BlueprintCallable)
	ETDCentaurWeaponState GetCentaurWeaponState() const;
	
	UFUNCTION(BlueprintCallable)
	bool IsStateGoodForShooting() const;
	
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override { /*Not Implemented for NPC*/ }

	void UpdateComponentsVisibility();
	
protected:
	virtual void BeginPlay() override;

	virtual void MeleeAttack_Implementation() override;

	
	//virtual UAnimMontage* GetCurrentMeleeAnimMontage_Implementation() const override;

protected:
	void Shoot_Internal();

	void SetWeaponState(ETDCentaurWeaponState NewState);
	
protected:
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Animation)
	UTDCentaurMontages* MyAnimMontages;

	
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USkeletalMeshComponent* BowIdle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USkeletalMeshComponent* BowAction;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USkeletalMeshComponent* Arrow;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USkeletalMeshComponent* BodyArmor;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TSubclassOf<class ATDProjectile> ArrowClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FName ArrowActionSocket_Name = TEXT("ArrowAction");
	
private:

	ETDCentaurWeaponState mCentaurWeaponState = ETDCentaurWeaponState::cwsNormal;	
};
