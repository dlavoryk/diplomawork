// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TDAbleToDie.h"
#include "TDShooter.h"
#include "TDDefenseBuilder.h"
#include "TDCharacter.generated.h"


DECLARE_MULTICAST_DELEGATE(FOnFire);

class UInputComponent;

/**
 * @todo dlavoryk: move logic to PlayerController
 * @todo dlavoryk: create simple model for updating UI, instead of a lot of different functions
 */
UCLASS(config = Game)
class DIPLOMATD_API ATDCharacter : public ACharacter, public ITDAbleToDie, public ITDShooter, public ITDDefenseBuilder
{
	GENERATED_BODY()

	DECLARE_EVENT(ATDCharacter, FTowerActionEvent);
	DECLARE_EVENT_TwoParams(ATDCharacter, FScoreEvent, int32, int32);
	
public:
	// Sets default values for this character's properties
	ATDCharacter();

	UFUNCTION(BlueprintCallable)
	bool IsFiring() const;

	virtual bool CanDie(float KillingDamage, FDamageEvent const& DamageEvent, AController* Killer, AActor* DamageCauser) const;
	virtual bool Die(float KillingDamage, struct FDamageEvent const& DamageEvent, class AController* Killer, class AActor* DamageCauser);

	
protected:
	// ITDAbleToDie interface

	virtual FTDAttribute GetHealthAttribute_Implementation() const override;
	virtual bool IsAlive_Implementation() const override;
	////
protected:
	// ITDShooter Interface

	virtual FTDWeaponAttributes GetWeaponConfig_Implementation() const override;
	virtual void ScoreKill_Implementation(AActor* KilledActor, int32 Points) override;
	virtual int32 GetPoints_Implementation() const override;
	virtual void SubscribeOnScoreIncreased_Implementation(UObject* Subcriber, FName UFunctionName) override;
	
	////

protected:
	// ITDTowerBuilder Interface
	virtual FTDAttribute GetTowerAttribute_Implementation() const override;
	virtual FTDAttribute GetBombsAttribute_Implementation() const override;
	
	virtual void SubscribeOnTowerSpawned_Implementation(UObject* Object, FName FunctionName) override;
	////

	virtual void SubscribeOnBombSpawnEvent_Implementation(UObject* Object, FName FunctionName) override;
	
	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	virtual void OnStartFire();
	virtual void OnStopFire();

	void OnStartReloading();
	
	void OnStartBuildingTower();
	void OnTowerBuilding();
	void OnTowerBuilt();
	void OnCancelBuildingTower();
	
	void DestroyTowerGhost();
	void ContiniousTowerBuilding(class AStaticMeshActor * Ghost);
	void FinishBuildingTower(class AStaticMeshActor* Ghost);
	void DecreaseTowerCounter();

	bool CanBuildTower() const;
	
	void MoveForward(float Value);
	void MoveRight(float Value);
	void TurnAtRate(float Rate);
	void LookUpAtRate(float Rate);


	void SpawnDefaultInventory();

	// BombSpawner function
	void OnBombSpawn();
	
public:
	FOnFire OnStartFire_Event;
	FOnFire OnStopFire_Event;

	
	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseLookUpRate;

	/** Gun muzzle's offset from the characters location */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	FVector GunOffset;

	// Current health of the Pawn
	//, Replicated
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Health)
	FTDAttribute Health;

	
	

	
private:
	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FirstPersonCameraComponent;

	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USkeletalMeshComponent* Mesh1P;

	
	
protected:
	UPROPERTY(EditAnywhere, Category = "BombSpawner")
	class UTDBombSpawner_AC* BombSpawnerComponent;
	
	/** default inventory list */
	UPROPERTY(EditDefaultsOnly, Category = Inventory)
	TArray<TSubclassOf<class ATDWeapon>> DefaultInventoryClasses;
	
	/** weapons in inventory */
	UPROPERTY(Transient)
	TArray<class ATDWeapon*> Inventory;
	//, Replicated
	
	/** currently equipped weapon */
	UPROPERTY(Transient)
	class ATDWeapon* CurrentWeapon;
	//, ReplicatedUsing = OnRep_CurrentWeapon

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<AStaticMeshActor> TowerGhostClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<AActor> TowerClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float PlaceTowerRadius = 1000.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FTDAttribute TowersAttribute;
	
	
	AStaticMeshActor* TowerGhost = nullptr;


	FTowerActionEvent OnTowerSpawned;
	FTowerActionEvent OnTowerBought;

	FTowerActionEvent OnBombSpawnEvent;
	

	FScoreEvent OnScored;
	
	uint8 bIsFiring : 1;
	uint8 bIsDying : 1;

};
