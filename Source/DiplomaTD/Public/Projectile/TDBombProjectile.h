// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile/TDProjectile.h"
#include "TDBombProjectile.generated.h"

/**
 * 
 */
UCLASS()
class DIPLOMATD_API ATDBombProjectile : public ATDProjectile
{
	GENERATED_BODY()

protected:
	virtual void PostInitializeComponents() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
public:
	void BombExplode();
};
