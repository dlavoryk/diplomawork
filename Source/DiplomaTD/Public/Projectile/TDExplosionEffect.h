// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/PointLightComponent.h"
#include "TDExplosionEffect.generated.h"

class USoundCue;
class UMaterial;

USTRUCT()
struct FTDDecalData
{
	GENERATED_USTRUCT_BODY()

	/** material */
	UPROPERTY(EditDefaultsOnly, Category = Decal) UMaterial* DecalMaterial;

	/** quad size (width & height) */
	UPROPERTY(EditDefaultsOnly, Category = Decal) float DecalSize;

	/** lifespan */
	UPROPERTY(EditDefaultsOnly, Category = Decal) float LifeSpan;

	/** defaults */
	FTDDecalData()
		: DecalMaterial(nullptr)
		, DecalSize(256.f)
		, LifeSpan(10.f)
	{
	}
};



UCLASS()
class DIPLOMATD_API ATDExplosionEffect : public AActor
{
	GENERATED_UCLASS_BODY()


		/** explosion FX */
		UPROPERTY(EditDefaultsOnly, Category = Effect) UParticleSystem* ExplosionFX;

private:
	/** explosion light */
	UPROPERTY(VisibleDefaultsOnly, Category = Effect) UPointLightComponent* ExplosionLight;

public:
	/** how long keep explosion light on? */
	UPROPERTY(EditDefaultsOnly, Category=Effect)
	float ExplosionLightFadeOut;

	/** explosion sound */
	UPROPERTY(EditDefaultsOnly, Category=Effect)
	USoundCue * ExplosionSound;
	
	/** explosion decals */
	UPROPERTY(EditDefaultsOnly, Category=Effect)
	FTDDecalData Decal;

	/** surface data for spawning */
	UPROPERTY(BlueprintReadOnly, Category=Surface)
	FHitResult SurfaceHit;

	UPROPERTY(EditDefaultsOnly, Category = Effect)
		FVector EmitterScale = FVector::OneVector;


	/** update fading light */
	virtual void Tick(float DeltaSeconds) override;

protected:
	/** spawn explosion */
	virtual void BeginPlay() override;

private:
	/** Point light component name */
	FName ExplosionLightComponentName;

public:
	/** Returns ExplosionLight subobject **/
	FORCEINLINE UPointLightComponent* GetExplosionLight() const { return ExplosionLight; }
	
};