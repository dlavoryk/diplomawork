#pragma once
#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "TDProjectileWeaponData.generated.h"

USTRUCT()
struct FTDProjectileWeaponData
{
	GENERATED_USTRUCT_BODY()

		/** projectile class */
	UPROPERTY(EditDefaultsOnly, Category = Projectile) TSubclassOf<class ATDProjectile> ProjectileClass;

	/** life time */
	UPROPERTY(EditDefaultsOnly, Category = Projectile) float ProjectileLife;

	/** damage at impact point */
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat) int32 ExplosionDamage;

	/** radius of damage */
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat) float ExplosionRadius;

	/** type of damage */
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat) TSubclassOf<UDamageType> DamageType;

	/** defaults */
	FTDProjectileWeaponData()
	{
		ProjectileClass = NULL;
		ProjectileLife = 10.0f;
		ExplosionDamage = 100;
		ExplosionRadius = 300.0f;
		DamageType = UDamageType::StaticClass();
	}
};
