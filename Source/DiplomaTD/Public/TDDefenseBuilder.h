// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Core/TDAttribute.h"
#include "TDDefenseBuilder.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTDDefenseBuilder : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class DIPLOMATD_API ITDDefenseBuilder
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FTDAttribute GetTowerAttribute() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FTDAttribute GetBombsAttribute() const;

	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void SubscribeOnTowerSpawned(UObject * Object, FName FunctionName);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void SubscribeOnBombSpawnEvent(UObject* Object, FName FunctionName);
	
};
