// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Core/TDAttribute.h"
#include "TDAbleToDie.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTDAbleToDie : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class DIPLOMATD_API ITDAbleToDie
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	struct FTDAttribute GetHealthAttribute() const;
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	bool IsAlive() const;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	int32 GetPointsForMyHead() const;

	
};
