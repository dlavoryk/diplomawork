// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TDPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class DIPLOMATD_API ATDPlayerController : public APlayerController
{
	GENERATED_BODY()

public:

	ATDPlayerController();


	bool IsUIRefreshRequested() const;

	void RequestUIRefresh();
	
protected:
	
	virtual void SetupInputComponent() override;

	virtual void OnPossess(APawn* InPawn) override;
	virtual void OnUnPossess() override;

	
protected:
	void RequestUIRefresh_Impl();
	
	void RefreshUI();
	void RefreshUI_Implementation();
	

	UFUNCTION() void OnTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

private:
	UFUNCTION() void OnTowerSpawned();
	UFUNCTION() void OnBombCountUpdate();
	UFUNCTION() void OnScoreChanged();

	UFUNCTION() void OnStartFire();
	UFUNCTION() void OnStopFire();

private:
	FTimerHandle mUIRefresh_Handle;
	bool bLoopRefresh = false;
	
};
