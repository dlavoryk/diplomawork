// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Templates/SubclassOf.h"
#include "TDBombSpawner_AC.generated.h"



UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DIPLOMATD_API UTDBombSpawner_AC : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTDBombSpawner_AC();

	UFUNCTION(BlueprintCallable)
	void SpawnBomb(FVector Location, FVector Direction);
	
	UFUNCTION(BlueprintPure)
	int32 GetCurrentBombCount() const;
	
	UFUNCTION(BlueprintPure)
	int32 GetMaxBombCount() const;


protected:
	virtual void InitializeComponent() override;
	UPROPERTY(EditAnywhere) 
	TSubclassOf<class ATDBombProjectile> BombClass;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Config")
	int32 MaxBombCount = 3;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	int32 CurrentBombCount;
};
