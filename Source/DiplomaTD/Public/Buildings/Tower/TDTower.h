// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDAbleToDie.h"
#include "TDTower.generated.h"

class ATDWeapon;
UCLASS()
class DIPLOMATD_API ATDTower : public AActor, public ITDAbleToDie
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDTower();

	UFUNCTION(BlueprintCallable)
	void Init(ATDWeapon* Weapon, UStaticMeshComponent* WeaponHorizontalOrigin);
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	
	//Start of ITDAbleToDie interface
	virtual FTDAttribute GetHealthAttribute_Implementation() const override;
	virtual bool IsAlive_Implementation() const override;;
	//End of ITDAbleToDie interface

	UFUNCTION(BlueprintNativeEvent)
	void Die();
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	class ANPC * FindNearestTarget() const;
	
	//@todo dlavoryk: these functions can be static
	bool HasLOSToActorWithDistanceCheck(ANPC* ActorToCheck) const;
	bool HasLOSToActorWithoutDistanceCheck(ANPC* ActorToCheck) const;

	void SetTarget(ANPC* FutureTarget);
	
protected:
	void SetTarget_Internal(ANPC * FutureTarget);
public:
	void ResetCurrentTarget();
	
private:

	void WeaponPostInit();
	
	UFUNCTION() void OnTargetDie(AActor* Actor, EEndPlayReason::Type Reason);


	static float GetAngleInDegreesBetweenDirections(const FVector& Direction1, const FVector& Direction2);
	
protected:
	ATDWeapon* MyWeapon = nullptr;
	UStaticMeshComponent* MyWeaponHorizontalOrigin = nullptr;
	

	class ANPC* Target = nullptr;

	UPROPERTY(EditDefaultsOnly)
	float AttackRadius = 500.f;


	UPROPERTY(EditDefaultsOnly)
	float MaxWaitingForEnemyTime = 2.f;

	float mWaitingTime = 0.f;


	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTDAttribute Health = { 25, 25 };

	
};
