#pragma once
#include "CoreMinimal.h"

#include "TDAttribute.generated.h"


USTRUCT(BlueprintType)
struct FTDAttribute
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 BaseValue = 100;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 CurrentValue = 100;
};