// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Perception/AIPerceptionTypes.h"

#include "DiplomaTD/Core/TDChaseStates.h"

#include "NPCAIController.generated.h"

struct FAIStimulus;
/**
 * 
 */
UCLASS()
class DIPLOMATD_API ANPCAIController : public AAIController
{
	GENERATED_BODY()

public:
	ANPCAIController();

	UFUNCTION(BlueprintCallable)
	AActor* GetEnemy() const;

	UFUNCTION(BlueprintCallable)
	void SetEnemy(AActor* Enemy);

	UFUNCTION(BlueprintCallable)
	virtual bool GetNextPatrolPoint(const FVector& Origin, FVector& RandomLocation, float Radius, ANavigationData* NavData = nullptr, TSubclassOf<UNavigationQueryFilter> FilterClass = nullptr);


	UFUNCTION(BlueprintCallable)
	bool CanBite() const;
	
public:
	
	UFUNCTION(BlueprintCallable)
	virtual void AttackEnemy();

	
protected:
	virtual void OnPossess(APawn* InPawn) override;;

	virtual void BeginPlay() override;

	virtual void GameHasEnded(AActor* EndGameFocus, bool bIsWinner) override;

	UFUNCTION() void OnPerceptionUpdated(const TArray<AActor*>& UpdatedActors);

	UFUNCTION() void OnPerceptionUpdated_FromVideo(const TArray<AActor*>& UpdatedActors);

	
	void UpdateBlackboardPlayerData(AActor * Actor, bool bHasLineOfSight);
	void ResetBlackboardPlayerData();
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite) UAIPerceptionComponent* AIPerceptionComponent;

	
	UPROPERTY(transient, BlueprintReadWrite) UBlackboardComponent* BlackboardComp;
	UPROPERTY(transient, BlueprintReadWrite) class UBehaviorTreeComponent* BT;


	

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = BlackboardKeys, meta=(DisplayName="Has Line Of Sight"))
	FName HasLineOfSight_Name = TEXT("HasLineOfSight");

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = BlackboardKeys, meta = (DisplayName = "Enemy Actor"))
	FName EnemyActor_Name = TEXT("EnemyActor");

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = BlackboardKeys, meta=(DisplayName="Player Tag"))
	FName PlayerTag_Name = TEXT("Player");

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = BlackboardKeys, meta = (DisplayName = "Is Investigating"))
	FName IsInvestigating_Name = TEXT("IsInvestigating");


	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = BlackboardKeys, meta = (DisplayName = "Destination"))
	FName Destination_Name = TEXT("Destination");
	
	FTimerHandle PlayerHandle;


	ETDChaseStates ChaseStates;
};