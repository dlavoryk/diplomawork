// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "TDBTTask_FindPointNearEnemy.generated.h"

/**
 * 
 */
UCLASS()
class DIPLOMATD_API UTDBTTask_FindPointNearEnemy : public UBTTask_BlackboardBase
{
	GENERATED_BODY()



	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;


public:
	UPROPERTY(EditAnywhere) float SearchRadius = 200.f;
	UPROPERTY(EditAnywhere) float DistanceFromEnemy = 600.f;
};
