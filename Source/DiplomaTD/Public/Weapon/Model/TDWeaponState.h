#pragma once
#include "CoreMinimal.h"
#include "TDWeaponState.generated.h"

UENUM()
enum class ETDWeaponState : uint8
{
	wsIdle,
	wsReloading,
	wsFiring,
	wsEquipping
};

