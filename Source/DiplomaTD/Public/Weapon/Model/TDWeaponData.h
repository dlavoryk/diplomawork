#pragma once
#include "CoreMinimal.h"
#include "TDWeaponData.generated.h"

USTRUCT(BlueprintType)
struct FTDWeaponData
{
	GENERATED_BODY()
public:

	UPROPERTY(EditDefaultsOnly, Category = Ammo)
	bool bInfiniteAmmo;

	UPROPERTY(EditDefaultsOnly, Category = Ammo)
	bool bInfiniteClip;

	UPROPERTY(EditDefaultsOnly, Category = Ammo)
	int32 MaxAmmo;

	UPROPERTY(EditDefaultsOnly, Category = Ammo)
	int32 AmmoPerClip;

	UPROPERTY(EditDefaultsOnly, Category = Ammo)
	int32 InitialClips;

	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)
	float TimeBetweenShots;

	FTDWeaponData()
	{
		bInfiniteAmmo = false;
		bInfiniteClip = false;
		MaxAmmo = 100;
		AmmoPerClip = 20;
		InitialClips = 4;
		TimeBetweenShots = 0.2f;
	}

};


USTRUCT(BlueprintType)
struct FTDWeaponAttributes : public FTDWeaponData
{
	GENERATED_BODY()
public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Ammo)
	int32 CurrentAmmoInClip;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Ammo)
	int32 TotalAmmo;

	FTDWeaponAttributes(): CurrentAmmoInClip(0), TotalAmmo(0)
	{
	}


	FTDWeaponAttributes(const FTDWeaponData& WeaponData, int32 CurrentAmmoInClip, int32 TotalAmmo):
		  FTDWeaponData(WeaponData)
		, CurrentAmmoInClip(CurrentAmmoInClip)
		, TotalAmmo(TotalAmmo)
	{
	}
	
	
};