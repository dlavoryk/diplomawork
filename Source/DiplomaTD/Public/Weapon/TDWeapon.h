// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GameFramework/Actor.h"
#include "Projectile/TDProjectileWeaponData.h"
#include "Model/TDWeaponData.h"
#include "Model/TDWeaponState.h"
#include "TDWeapon.generated.h"


UCLASS( )
class DIPLOMATD_API ATDWeapon : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	ATDWeapon();

	void ApplyWeaponConfig(FTDProjectileWeaponData& Data);

	virtual void StartFire();
	virtual void StopFire();


	virtual void StartReload();
	
	virtual void ReloadWeapon();
	virtual void StopReloading();


	bool CanReload() const;
	bool CanFire() const;
	bool HasInfiniteClip() const;
	bool HasInfiniteAmmo() const;

	int32 GetCurrentAmmo() const;
	int32 GetCurrentAmmoInClip() const;
	const FTDWeaponData& GetWeaponConfig() const { return WeaponConfig; }
	
	
protected:
	virtual void PostInitProperties() override;;
	
	virtual void HandleFire();
	virtual void HandleReFire();
	virtual void SimulateFire();
	virtual FVector GetAdjustedAim() const;
public:
	FVector GetMuzzleLocation() const;
	/** get direction of weapon's muzzle */
	FVector GetMuzzleDirection() const;
protected:
	FHitResult WeaponTrace(const FVector& TraceFrom, const FVector& TraceTo) const;
	/** get the originating location for camera damage */
	FVector GetCameraDamageStartLocation(const FVector& AimDir) const;

	virtual void SpawnProjectile(FVector Location, FVector Direction);

	
	void OnBurstStarted();
	void OnBurstFinished();

	void UseAmmo();


protected:

	void SetWeaponState(ETDWeaponState NewState);
	ETDWeaponState GetCurrentState() const;


	void DetermineWeaponState();

	/** get weapon mesh (needs pawn owner to determine variant) */
	USkeletalMeshComponent* GetWeaponMesh() const;

	
protected:
	/** Handle for efficient management of StopReload timer */
	FTimerHandle TimerHandle_StopReload;

	/** Handle for efficient management of ReloadWeapon timer */
	FTimerHandle TimerHandle_ReloadWeapon;

	/** Handle for efficient management of HandleFiring timer */
	FTimerHandle TimerHandle_HandleFiring;


protected:

	/** time of last successful weapon fire */
	float LastFireTime;

	float TimerIntervalAdjustment = 0.f;
	UPROPERTY(EditDefaultsOnly)
	bool bAllowAutomaticWeaponCatchup = true;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	FTDWeaponData WeaponConfig;

	/** weapon config */
	UPROPERTY(EditDefaultsOnly, Category = Config)
	FTDProjectileWeaponData ProjectileConfig;
	
	UPROPERTY()
	int32 CurrentAmmo;

	UPROPERTY()
	int32 CurrentAmmoInClip;


	/** name of bone/socket for muzzle in weapon mesh */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	FName MuzzleAttachPoint = TEXT("Muzzle");

	
	uint8 bIsReloadRequested : 1;
	uint8 bIsFireRequested : 1;
private:
	/** weapon mesh: 1st person view */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh) USkeletalMeshComponent* Mesh1P;
	
private:
	ETDWeaponState mCurrentState;

	
};
