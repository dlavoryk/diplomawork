// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Weapon/Model/TDWeaponData.h"
#include "TDShooter.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTDShooter : public UInterface
{
	GENERATED_BODY()
};

/**
 * @todo dlavoryk: move points into separate interface
 */
class DIPLOMATD_API ITDShooter
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:


	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	FTDWeaponAttributes GetWeaponConfig() const;
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void ScoreKill(AActor * KilledActor, int32 Points);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	int32 GetPoints() const;

	/*
	 * Function should accept 2 x int32 arguments. First is Added Points, Second is Total Points
	 */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void SubscribeOnScoreIncreased(UObject * Subcriber, FName UFunctionName);

	
};
