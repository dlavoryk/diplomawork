// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class DiplomaTD : ModuleRules
{
	public DiplomaTD(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;


        PublicIncludePaths.AddRange(new string[]
        {
			"DiplomaTD/Public/"
        });

		PublicDependencyModuleNames.AddRange(new string[]
        {
            "Core", 
            "CoreUObject", 
            "Engine", 
            "InputCore", 
            "HeadMountedDisplay",
            "AIMODULE",
            "NAVIGATIONSYSTEM",
            "GAMEPLAYTASKS"
        });
	}
}
