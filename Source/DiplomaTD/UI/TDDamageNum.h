// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "TDDamageNum.generated.h"

/**
 * 
 */
UCLASS()
class DIPLOMATD_API UTDDamageNum : public UUserWidget
{
	GENERATED_BODY()


public:
	void SetDamageAmount(int32 Damage);

	
protected:

	UPROPERTY(meta = (BindWidget)) UTextBlock* DamageNumTextBlock;
};
