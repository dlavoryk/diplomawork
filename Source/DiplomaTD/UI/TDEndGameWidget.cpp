// Fill out your copyright notice in the Description page of Project Settings.


#include "TDEndGameWidget.h"

#include "Components/TextBlock.h"
#include "Components/WidgetSwitcher.h"

void UTDEndGameWidget::Display(const FTDEndGameModel& EndGameModel)
{
	TotalPoints->SetText(FText::AsNumber(EndGameModel.TotalPoints));
	TotalKills->SetText(FText::AsNumber(EndGameModel.TotalKills));
	WinLossTextSwitcher->SetActiveWidgetIndex(static_cast<int32>(EndGameModel.bIsWin));
}
