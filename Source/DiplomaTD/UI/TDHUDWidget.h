// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TDHUDWidgetInterface.h"
#include "TDHUDWidget.generated.h"

/**
 * 
 */
UCLASS()
class DIPLOMATD_API UTDHUDWidget : public UUserWidget, public ITDHUDWidgetInterface
{
	GENERATED_BODY()


protected:

	virtual void UpdateEquipment_Implementation(FTDUIEquipment Equipment) override;
	virtual void UpdateHealth_Implementation(FTDAttribute HealthAtt) override;
	virtual void OnScoreChanged_Implementation(int32 AddedPoints, int32 TotalPoints) override;

protected:
	static void SetSlashedText(class UTextBlock* TextBlock, int32 Before, int32 After);
	
	
protected:
	UPROPERTY(meta = (BindWidget))
	class UProgressBar * HealthBar;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock   * TowersCountText;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* AmmoTextBlock;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* MaxAmmoTextBlock;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* BombTextBlock;



protected:
};
