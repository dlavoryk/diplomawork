// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Blueprint/UserWidget.h"
#include "DiplomaTD/Core/TDEndGameModel.h"

#include "DiplomaTDHUD.generated.h"





UCLASS()
class ADiplomaTDHUD : public AHUD
{
	GENERATED_BODY()

	friend struct ITDAmmoUpdater;
	
public:
	ADiplomaTDHUD();

	virtual void PostInitializeComponents() override;
	
	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

	UFUNCTION(BlueprintCallable)
	void HandleMatchFinished(const FTDEndGameModel & EndGameModel);

	void OnScoreChanged(int32 AddedPoints, int32 TotalPoints);
	void UpdateEquipment(struct FTDUIEquipment Equipment);
	void UpdateHealth(struct FTDAttribute HealthAtt);


	void SetupEndGameWidget();
	
protected:
	
	UPROPERTY(EditDefaultsOnly, meta= (Implements = TDHUDWidgetInterface))
	TSubclassOf<UUserWidget> MainWidgetClass;
	UPROPERTY() UUserWidget* HUDWidget;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class UTDEndGameWidget> EndGameWidgetClass;
	UPROPERTY() class UTDEndGameWidget* EndGameWidget;
	
private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};
