// Fill out your copyright notice in the Description page of Project Settings.


#include "TDMainMenu.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"

void UTDMainMenu::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	StartGameButton->OnClicked.AddUniqueDynamic(this, &UTDMainMenu::OnStartGameButtonClicked);
	OptionsButton->OnClicked.AddUniqueDynamic(this, &UTDMainMenu::OnOptionsButtonClicked);
	QuitGameButton->OnClicked.AddUniqueDynamic(this, &UTDMainMenu::QuitGameButtonClicked);
	
}

void UTDMainMenu::OnStartGameButtonClicked()
{
	//@todo dlavoryk: add loading screen
	
	//@todo dlavoryk: OpenLevel should be delegated to other class if I add more than 1 levels
	UGameplayStatics::OpenLevel(this, GameLevelName);
}

void UTDMainMenu::OnOptionsButtonClicked()
{
	//@todo dlavoryk: open menu
}

void UTDMainMenu::QuitGameButtonClicked()
{
	UKismetSystemLibrary::QuitGame(this, GetOwningPlayer(), EQuitPreference::Quit, false);
}
