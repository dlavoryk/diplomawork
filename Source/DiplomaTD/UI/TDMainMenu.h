// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "TDMainMenu.generated.h"

/**
 * 
 */
UCLASS()
class DIPLOMATD_API UTDMainMenu : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual void NativeOnInitialized() override;


protected:
	UFUNCTION() void OnStartGameButtonClicked();
	UFUNCTION() void OnOptionsButtonClicked();
	UFUNCTION() void QuitGameButtonClicked();
	

public:
	UPROPERTY(EditAnywhere) FName GameLevelName;
	
protected:

	
	UPROPERTY(meta = (BindWidget)) UButton* StartGameButton;
	UPROPERTY(meta = (BindWidget)) UButton* OptionsButton;
	UPROPERTY(meta = (BindWidget)) UButton* QuitGameButton;
	
};
