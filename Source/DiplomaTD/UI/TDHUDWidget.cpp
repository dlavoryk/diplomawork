// Fill out your copyright notice in the Description page of Project Settings.


#include "TDHUDWidget.h"
#include "Components/ProgressBar.h"
#include "Components/TextBlock.h"

void UTDHUDWidget::UpdateEquipment_Implementation(FTDUIEquipment Equipment)
{
	MaxAmmoTextBlock->SetText(FText::AsNumber(Equipment.AmmoConfig.TotalAmmo - Equipment.AmmoConfig.CurrentAmmoInClip));

	SetSlashedText(AmmoTextBlock, Equipment.AmmoConfig.CurrentAmmoInClip, Equipment.AmmoConfig.AmmoPerClip);
	SetSlashedText(BombTextBlock, Equipment.BombConfig.CurrentValue, Equipment.BombConfig.BaseValue);
	SetSlashedText(TowersCountText, Equipment.TowersConfig.CurrentValue, Equipment.TowersConfig.BaseValue);
}

void UTDHUDWidget::UpdateHealth_Implementation(FTDAttribute HealthAtt)
{
	HealthBar->SetPercent(static_cast<float>(HealthAtt.CurrentValue) / HealthAtt.BaseValue);
}

void UTDHUDWidget::OnScoreChanged_Implementation(int32 AddedPoints, int32 TotalPoints)
{
	
}

void UTDHUDWidget::SetSlashedText(UTextBlock* TextBlock, int32 Before, int32 After)
{
	TextBlock->SetText(FText::FromString(FString::Printf(TEXT("%i/%i"), Before, After)));
}
