// Fill out your copyright notice in the Description page of Project Settings.


#include "TDDamageNum.h"

void UTDDamageNum::SetDamageAmount(int32 Damage)
{
	DamageNumTextBlock->SetText(FText::AsNumber(Damage));
}
