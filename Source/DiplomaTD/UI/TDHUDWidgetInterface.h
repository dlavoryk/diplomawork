// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Components/Widget.h"
#include "Core/TDAttribute.h"
#include "Weapon/Model/TDWeaponData.h"
#include "TDHUDWidgetInterface.generated.h"

USTRUCT(BlueprintType)
struct FTDUIAmmoConfig
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere)
	int32 TotalAmmoCount = 0;

	UPROPERTY(EditAnywhere)
	int32 MaxAmmoInClip  = 0;

	UPROPERTY(EditAnywhere)
	int32 AmmoInClip     = 0;

};

USTRUCT(BlueprintType)
struct FTDUIEquipment
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTDWeaponAttributes AmmoConfig;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTDAttribute TowersConfig;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTDAttribute BombConfig;

	FTDUIEquipment() = default;

	FTDUIEquipment(FTDWeaponAttributes Ammo, FTDAttribute Tower, FTDAttribute Bomd) :
		  AmmoConfig(Ammo)
		, TowersConfig(Tower)
		, BombConfig(Bomd)
	{
	}
	
};

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTDHUDWidgetInterface : public UInterface
{
	GENERATED_BODY()
};


/**
 * 
 */
class DIPLOMATD_API ITDHUDWidgetInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void UpdateHealth(FTDAttribute HealthAtt);

	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void UpdateEquipment(FTDUIEquipment Equipment);
	

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnScoreChanged(int32 AddedPoints, int32 TotalPoints);

	
};
