
// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "DiplomaTDHUD.h"
#include "Engine/Canvas.h"
#include "Engine/Texture2D.h"
#include "TextureResource.h"
#include "CanvasItem.h"
#include "UObject/ConstructorHelpers.h"
#include "TDHUDWidgetInterface.h"
#include "TDEndGameWidget.h"
#include "DiplomaTD/Core/TDGameInstance.h"
#include "TimerManager.h"


ADiplomaTDHUD::ADiplomaTDHUD()
{
	// Set the crosshair texture
	static ConstructorHelpers::FObjectFinder<UTexture2D> CrosshairTexObj(TEXT("/Game/FirstPerson/Textures/FirstPersonCrosshair"));
	CrosshairTex = CrosshairTexObj.Object;
}

void ADiplomaTDHUD::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	HUDWidget = CreateWidget<UUserWidget>(this->GetOwningPlayerController(), MainWidgetClass);
	if (!HUDWidget)
	{
		return;
	}
	HUDWidget->AddToViewport(1);
}

void ADiplomaTDHUD::DrawHUD()
{
	Super::DrawHUD();

	// Draw very simple crosshair

	// find center of the Canvas
	const FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);

	// offset by half the texture's dimensions so that the center of the texture aligns with the center of the Canvas
	const FVector2D CrosshairDrawPosition( (Center.X),
										   (Center.Y + 20.0f));

	// draw the crosshair
	FCanvasTileItem TileItem( CrosshairDrawPosition, CrosshairTex->Resource, FLinearColor::White);
	TileItem.BlendMode = SE_BLEND_Translucent;
	Canvas->DrawItem( TileItem );

}

void ADiplomaTDHUD::HandleMatchFinished(const FTDEndGameModel& EndGameModel)
{
	EndGameWidget = CreateWidget<UTDEndGameWidget>(GetWorld(), EndGameWidgetClass);
	check(EndGameWidget);
	SetupEndGameWidget();
	EndGameWidget->AddToViewport(2);
	EndGameWidget->Display(EndGameModel);
}

void ADiplomaTDHUD::OnScoreChanged(int32 AddedPoints, int32 TotalPoints)
{
	ITDHUDWidgetInterface::Execute_OnScoreChanged(HUDWidget, AddedPoints, TotalPoints);
}

void ADiplomaTDHUD::UpdateEquipment(FTDUIEquipment Equipment)
{
	ITDHUDWidgetInterface::Execute_UpdateEquipment(HUDWidget, Equipment);
}

void ADiplomaTDHUD::UpdateHealth(FTDAttribute HealthAtt)
{
	ITDHUDWidgetInterface::Execute_UpdateHealth(HUDWidget, HealthAtt);
}

void ADiplomaTDHUD::SetupEndGameWidget()
{
	auto MyGM = GetWorld()->GetGameInstanceChecked<UTDGameInstance>();
	EndGameWidget->AddButtonClickedListener<ETDEndGameButtons::egbMainMenu>(MyGM, &UTDGameInstance::LoadMainMenu, TEXT("LoadMainMenu"));
	EndGameWidget->AddButtonClickedListener<ETDEndGameButtons::egbNext>(MyGM, &UTDGameInstance::LoadNextLevel, TEXT("LoadNextLevel"));
	EndGameWidget->AddButtonClickedListener<ETDEndGameButtons::egbRestart>(MyGM, &UTDGameInstance::RestartCurrentLevel, TEXT("RestartCurrentLevel"));
}
