// Fill out your copyright notice in the Description page of Project Settings.


#include "TDShowDamage_Component.h"
#include "GameFramework/Actor.h"
#include "Components/WidgetComponent.h"
#include "TDDamageNum.h"
#include "TimerManager.h"
#include "Kismet/KismetSystemLibrary.h"

// Sets default values for this component's properties
UTDShowDamage_Component::UTDShowDamage_Component()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UTDShowDamage_Component::BeginPlay()
{
	Super::BeginPlay();

	// ...
	bWantsInitializeComponent = true;	
}

void UTDShowDamage_Component::OnTakeAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
	AController* InstigatedBy, AActor* DamageCauser)
{
	UActorComponent* AttachedComponent = GetOwner()->AddComponent(TEXT("DamageNum"), true, GetOwner()->GetActorTransform(), UWidgetComponent::StaticClass());
	UWidgetComponent* MyWidgetComponent = Cast<UWidgetComponent>(AttachedComponent);
	MyWidgetComponent->SetWidgetClass(DamageNumUIClass);
	Cast<UTDDamageNum >(MyWidgetComponent->GetUserWidgetObject())->SetDamageAmount(Damage);

	FLatentActionInfo LatentActionInfo;
	LatentActionInfo.CallbackTarget = MyWidgetComponent;
	LatentActionInfo.ExecutionFunction = GET_FUNCTION_NAME_CHECKED(UWidgetComponent, DestroyComponent);
	UKismetSystemLibrary::Delay(this, 1.f, LatentActionInfo);
	
}

