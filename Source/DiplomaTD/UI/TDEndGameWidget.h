// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "DiplomaTD/Core/TDEndGameModel.h"
#include "TDEndGameWidget.generated.h"





UENUM(BlueprintType)
enum class ETDEndGameButtons : uint8
{
	egbMainMenu,
	egbRestart,
	egbNext
};

/**
 * 
 */
UCLASS()
class DIPLOMATD_API UTDEndGameWidget : public UUserWidget
{
	GENERATED_BODY()


public:

	UFUNCTION(BlueprintCallable)
	void Display(const FTDEndGameModel & EndGameModel);

	// Add button clicked listener. prevents duplicates
	template<ETDEndGameButtons Type, typename USerObject, typename UFunction>
	void AddButtonClickedListener(USerObject* Obj, UFunction Function, FName FunctionName);

	
	template<ETDEndGameButtons Type, typename USerObject>
	void ClearButtonClickedListeners(USerObject* Obj);

private:
	
protected:


	UPROPERTY(meta = (BindWidget)) class UTextBlock* TotalPoints;
	UPROPERTY(meta = (BindWidget)) class UTextBlock* TotalKills;

	UPROPERTY(meta = (BindWidget)) class UButton* RestartButton;
	UPROPERTY(meta = (BindWidget)) class UButton* MainMenuButton;
	UPROPERTY(meta = (BindWidget)) class UButton* NextButton;

	UPROPERTY(meta = (BindWidget)) class UWidgetSwitcher* WinLossTextSwitcher;


};

template <ETDEndGameButtons Type, typename USerObject, typename UFunction>
void UTDEndGameWidget::AddButtonClickedListener(USerObject* Obj, UFunction Function, FName FunctionName)
{
	switch (Type)
	{
	case ETDEndGameButtons::egbMainMenu:
		MainMenuButton->OnClicked.__Internal_AddUniqueDynamic(Obj, Function, FunctionName);
		break;
	case ETDEndGameButtons::egbNext:
		NextButton->OnClicked.__Internal_AddUniqueDynamic(Obj, Function, FunctionName);
		break;;
	case ETDEndGameButtons::egbRestart:
		 RestartButton->OnClicked.__Internal_AddUniqueDynamic(Obj, Function, FunctionName);
		break;;
	}
}

template <ETDEndGameButtons Type, typename USerObject>
void UTDEndGameWidget::ClearButtonClickedListeners(USerObject* Obj)
{
	switch (Type)
	{
	case ETDEndGameButtons::egbMainMenu:
		MainMenuButton->OnClicked.RemoveAll(Obj);
		break;
	case ETDEndGameButtons::egbNext:
		NextButton->OnClicked.RemoveAll(Obj);
		break;;
	case ETDEndGameButtons::egbRestart:
		RestartButton->OnClicked.RemoveAll(Obj);
		break;;
	}
}
